class CreateEventTemplateCuisineTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :event_template_cuisine_types do |t|
      t.references :cuisine_type, foreign_key: true
      t.references :event_template, foreign_key: true

      t.timestamps
    end

    add_index :event_template_cuisine_types, [:cuisine_type_id, :event_template_id], unique: true, name: 'index_unique_event_template_cusines_types'
  end
end
