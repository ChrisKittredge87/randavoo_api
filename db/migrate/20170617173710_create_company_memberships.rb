class CreateCompanyMemberships < ActiveRecord::Migration[5.1]
  def change
    create_table :company_memberships do |t|
      t.references :role, foreign_key: true, index: true, null: false
      t.references :company, foreign_key: true, index: true, null: false, on_delete: :cascade
      t.references :user, foreign_key: true, index: true, null: false, on_delete: :cascade

      t.timestamps
    end

    add_index :company_memberships, [:user_id, :company_id], unique: true
  end
end
