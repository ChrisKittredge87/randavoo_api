class ChangeDecimalsToFloats < ActiveRecord::Migration[5.1]
  def change
    change_column :event_templates, :overall_budget, :float, null: true
    change_column :event_templates, :venue_budget, :float, null: true
    change_column :event_templates, :vendor_budget, :float, null: false
    change_column :event_templates, :budget_per_caterer, :float, null: true
    change_column :event_templates, :budget_per_food_truck, :float, null: true
  end
end
