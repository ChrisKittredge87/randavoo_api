class CreateVenueExperiences < ActiveRecord::Migration[5.1]
  def change
    create_table :venue_experiences do |t|
      t.string :description, limit: 500
      t.string :name, limit: 100, null: false
      t.string :display_name, limit: 100, null: false
      
      t.timestamps
    end
  end
end
