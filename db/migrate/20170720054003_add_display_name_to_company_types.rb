class AddDisplayNameToCompanyTypes < ActiveRecord::Migration[5.1]
  def change
    add_column :company_types, :display_name, :string, limit: 100
  end
end
