class CreateCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :companies do |t|
      t.string :name, limit: 250, index: true, null: false
      t.references :company_type, foreign_key: true, null: false, index: true
      t.references :address, foreign_key: true, null: true, index: true

      t.timestamps
    end

    add_reference :companies, :parent, index: true
  end
end
