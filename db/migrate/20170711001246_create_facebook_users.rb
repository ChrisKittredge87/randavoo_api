class CreateFacebookUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :facebook_users do |t|
      t.references :user
      t.bigint :facebook_id

      t.timestamps
    end
  end
end
