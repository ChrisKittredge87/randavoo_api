class CreateServiceProviderGalleries < ActiveRecord::Migration[5.1]
  def change
    create_table :service_provider_galleries do |t|
      t.string :name, limit: 100, null: false
      t.string :description, limit: 250
      t.references :service_provider, foreign_key: true, index: true, on_delete: :cascade

      t.timestamps
    end
  end
end
