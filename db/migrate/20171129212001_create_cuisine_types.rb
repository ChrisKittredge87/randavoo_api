class CreateCuisineTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :cuisine_types do |t|
      t.string :description, limit: 500, null: false
      t.string :name, limit: 100, null: false
      t.string :display_name, limit: 100, null: false

      t.timestamps
    end
  end
end
