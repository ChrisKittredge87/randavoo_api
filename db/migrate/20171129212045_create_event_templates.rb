class CreateEventTemplates < ActiveRecord::Migration[5.1]
  def change
    create_table :event_templates do |t|
      t.references :user, foreign_key: true, null: false
      t.references :company, foreign_key: true, null: false
      # Date Budget Time
      t.boolean :dont_set_budget, null: false
      t.datetime :start_date, null: false
      t.datetime :end_date, null: false
      t.integer :number_of_expected_guests, null: false
      t.decimal :overall_budget, null: true
      # Venue
      t.st_point :lonlat, geographic: true, null: false
      t.string :address, null: false
      t.boolean :include_in_house_vendors, null: false
      t.boolean :include_outside_vendors, null: false
      t.boolean :needs_venue, null: false
      t.integer :radius, null: true
      t.decimal :venue_budget, null: true
      t.references :venue_experience, foreign_key: true, null: true
      # Vendors
      t.decimal :vendor_budget, null: false
      t.integer :caterer_quantity, null: false
      t.decimal :budget_per_caterer, null: true
      t.integer :food_truck_quantity, null: false
      t.decimal :budget_per_food_truck, null: true
      t.boolean :food_truck_paid_for_by_host, null: true
      # Name and Description
      t.string :description, limit: 500, null: false
      t.string :name, limit: 100, null: false

      t.timestamps
    end

    add_index :event_templates, :lonlat, using: :gist
  end
end
