class CreateCompanyTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :company_types do |t|
      t.string :name, limit: 50, null: false
      t.string :description, limit: 150, null: false

      t.timestamps
    end
  end
end
