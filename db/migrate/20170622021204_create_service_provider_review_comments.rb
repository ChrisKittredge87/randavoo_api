class CreateServiceProviderReviewComments < ActiveRecord::Migration[5.1]
  def change
    create_table :service_provider_review_comments do |t|
      t.string :text, limit: 2000, null: false
      t.references :service_provider_review, foreign_key: true, index: { name: 'index_sp_rc_on_sp_review_id' }
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
