class AddDisplayNameToServiceProviderType < ActiveRecord::Migration[5.1]
  def up
    add_column :service_provider_types, :display_name, :string, limit: 100
    ServiceProviderType.find_each { |sp|
      sp.display_name = sp.name.split('_').map(&:capitalize).join(' ')
      sp.save!
    }
    change_column :service_provider_types, :display_name, :string, null: false
  end

  def down
    remove_column :service_provider_types, :display_name, :string, limit: 100
  end
end
