class CreateServiceProviderPictures < ActiveRecord::Migration[5.1]
  def change
    create_table :service_provider_pictures do |t|
      t.string :name, limit: 100
      t.string :description, limit: 250
      t.attachment :image, null: false
      t.references :service_provider_gallery, foreign_key: true, index: true, on_delete: :cascade
      t.timestamps
    end
  end
end
