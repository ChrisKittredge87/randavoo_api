class CreateGoogleUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :google_users do |t|
      t.references :user
      t.bigint :google_id

      t.timestamps
    end
  end
end
