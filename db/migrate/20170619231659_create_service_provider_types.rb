class CreateServiceProviderTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :service_provider_types do |t|
      t.string :name, limit: 250, null: false
      t.string :description, limit: 500, null: false

      t.timestamps
    end
  end
end
