class CreateServiceProviderReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :service_provider_reviews do |t|
      t.integer :rating, null: false
      t.string :header, limit: 150, null: false
      t.string :text, limit: 2000, null: false
      t.references :service_provider, foreign_key: true, index: true, null: false
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
