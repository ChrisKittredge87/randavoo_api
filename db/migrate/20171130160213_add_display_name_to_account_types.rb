class AddDisplayNameToAccountTypes < ActiveRecord::Migration[5.1]
  def change
    add_column :account_types, :display_name, :string, limit: 100
    AccountType.update_all(display_name: '')
    change_column :account_types, :display_name, :string, limit: 100, null: false
  end
end
