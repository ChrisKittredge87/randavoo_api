class CreateServiceProviders < ActiveRecord::Migration[5.1]
  def change
    create_table :service_providers do |t|
      t.string :name, limit: 250
      t.string :overview, limit: 5000
      t.float :travel_distance, null: true
      t.references :company, foreign_key: true, index: true, null: false, on_delete: :cascade
      t.references :service_provider_type, foreign_key: true, index: true, null: false
      t.references :address, foreign_key: true, index: true, null: true

      t.timestamps
    end
  end
end
