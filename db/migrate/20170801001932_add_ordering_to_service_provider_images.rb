class AddOrderingToServiceProviderImages < ActiveRecord::Migration[5.1]
  def change
    add_column :service_provider_pictures, :order, :integer, null: false, default: 0
  end
end
