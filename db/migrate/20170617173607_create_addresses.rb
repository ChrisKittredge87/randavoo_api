class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.string :street_address_1, limit: 200, null: false
      t.string :street_address_2, limit: 200, null: true
      t.string :locality, limit: 200, null: false
      t.string :region, limit: 200, null: false
      t.string :postcode, limit: 100, null: false
      t.string :country, limit: 200, null: false
      t.st_point :lonlat, geographic: true

      t.timestamps
    end

    add_index :addresses, :lonlat, using: :gist
  end
end
