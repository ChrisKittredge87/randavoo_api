# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171203192853) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "account_types", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.string "description", limit: 150, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "display_name", limit: 100, null: false
  end

  create_table "addresses", force: :cascade do |t|
    t.string "street_address_1", limit: 200, null: false
    t.string "street_address_2", limit: 200
    t.string "locality", limit: 200, null: false
    t.string "region", limit: 200, null: false
    t.string "postcode", limit: 100, null: false
    t.string "country", limit: 200, null: false
    t.geography "lonlat", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lonlat"], name: "index_addresses_on_lonlat", using: :gist
  end

  create_table "companies", force: :cascade do |t|
    t.string "name", limit: 250, null: false
    t.bigint "company_type_id", null: false
    t.bigint "address_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "parent_id"
    t.index ["address_id"], name: "index_companies_on_address_id"
    t.index ["company_type_id"], name: "index_companies_on_company_type_id"
    t.index ["name"], name: "index_companies_on_name"
    t.index ["parent_id"], name: "index_companies_on_parent_id"
  end

  create_table "company_memberships", force: :cascade do |t|
    t.bigint "role_id", null: false
    t.bigint "company_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_company_memberships_on_company_id"
    t.index ["role_id"], name: "index_company_memberships_on_role_id"
    t.index ["user_id", "company_id"], name: "index_company_memberships_on_user_id_and_company_id", unique: true
    t.index ["user_id"], name: "index_company_memberships_on_user_id"
  end

  create_table "company_types", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.string "description", limit: 150, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "display_name", limit: 100
  end

  create_table "cuisine_types", force: :cascade do |t|
    t.string "description", limit: 500, null: false
    t.string "name", limit: 100, null: false
    t.string "display_name", limit: 100, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "event_template_cuisine_types", force: :cascade do |t|
    t.bigint "cuisine_type_id"
    t.bigint "event_template_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cuisine_type_id", "event_template_id"], name: "index_unique_event_template_cusines_types", unique: true
    t.index ["cuisine_type_id"], name: "index_event_template_cuisine_types_on_cuisine_type_id"
    t.index ["event_template_id"], name: "index_event_template_cuisine_types_on_event_template_id"
  end

  create_table "event_templates", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "company_id", null: false
    t.boolean "dont_set_budget", null: false
    t.datetime "start_date", null: false
    t.datetime "end_date", null: false
    t.integer "number_of_expected_guests", null: false
    t.float "overall_budget"
    t.geography "lonlat", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}, null: false
    t.string "address", null: false
    t.boolean "include_in_house_vendors", null: false
    t.boolean "include_outside_vendors", null: false
    t.boolean "needs_venue", null: false
    t.integer "radius"
    t.float "venue_budget"
    t.bigint "venue_experience_id"
    t.float "vendor_budget", null: false
    t.integer "caterer_quantity", null: false
    t.float "budget_per_caterer"
    t.integer "food_truck_quantity", null: false
    t.float "budget_per_food_truck"
    t.boolean "food_truck_paid_for_by_host"
    t.string "description", limit: 500, null: false
    t.string "name", limit: 100, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_event_templates_on_company_id"
    t.index ["lonlat"], name: "index_event_templates_on_lonlat", using: :gist
    t.index ["user_id"], name: "index_event_templates_on_user_id"
    t.index ["venue_experience_id"], name: "index_event_templates_on_venue_experience_id"
  end

  create_table "facebook_users", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "facebook_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_facebook_users_on_user_id"
  end

  create_table "google_users", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "google_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_google_users_on_user_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name", null: false
    t.string "description", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "display_name", limit: 100
    t.index ["name"], name: "index_roles_on_name", unique: true
  end

  create_table "service_provider_galleries", force: :cascade do |t|
    t.string "name", limit: 100, null: false
    t.string "description", limit: 250
    t.bigint "service_provider_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["service_provider_id"], name: "index_service_provider_galleries_on_service_provider_id"
  end

  create_table "service_provider_pictures", force: :cascade do |t|
    t.string "name", limit: 100
    t.string "description", limit: 250
    t.string "image_file_name", null: false
    t.string "image_content_type", null: false
    t.integer "image_file_size", null: false
    t.datetime "image_updated_at", null: false
    t.bigint "service_provider_gallery_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "order", default: 0, null: false
    t.index ["service_provider_gallery_id"], name: "index_service_provider_pictures_on_service_provider_gallery_id"
  end

  create_table "service_provider_review_comments", force: :cascade do |t|
    t.string "text", limit: 2000, null: false
    t.bigint "service_provider_review_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["service_provider_review_id"], name: "index_sp_rc_on_sp_review_id"
    t.index ["user_id"], name: "index_service_provider_review_comments_on_user_id"
  end

  create_table "service_provider_reviews", force: :cascade do |t|
    t.integer "rating", null: false
    t.string "header", limit: 150, null: false
    t.string "text", limit: 2000, null: false
    t.bigint "service_provider_id", null: false
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["service_provider_id"], name: "index_service_provider_reviews_on_service_provider_id"
    t.index ["user_id"], name: "index_service_provider_reviews_on_user_id"
  end

  create_table "service_provider_types", force: :cascade do |t|
    t.string "name", limit: 250, null: false
    t.string "description", limit: 500, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "display_name", null: false
  end

  create_table "service_providers", force: :cascade do |t|
    t.string "name", limit: 250
    t.string "overview", limit: 5000
    t.float "travel_distance"
    t.bigint "company_id", null: false
    t.bigint "service_provider_type_id", null: false
    t.bigint "address_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["address_id"], name: "index_service_providers_on_address_id"
    t.index ["company_id"], name: "index_service_providers_on_company_id"
    t.index ["service_provider_type_id"], name: "index_service_providers_on_service_provider_type_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name", limit: 250, null: false
    t.string "last_name", limit: 250, null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "auth_token", default: ""
    t.datetime "token_expiry", default: "2017-07-09 21:24:30", null: false
    t.boolean "is_admin", default: false
    t.bigint "account_type_id", null: false
    t.datetime "deleted_at"
    t.datetime "datetime"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "manager_id"
    t.index ["account_type_id"], name: "index_users_on_account_type_id"
    t.index ["auth_token"], name: "index_users_on_auth_token", unique: true
    t.index ["deleted_at"], name: "index_users_on_deleted_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["manager_id"], name: "index_users_on_manager_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "venue_experiences", force: :cascade do |t|
    t.string "description", limit: 500
    t.string "name", limit: 100, null: false
    t.string "display_name", limit: 100, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "companies", "addresses"
  add_foreign_key "companies", "company_types"
  add_foreign_key "company_memberships", "companies"
  add_foreign_key "company_memberships", "roles"
  add_foreign_key "company_memberships", "users"
  add_foreign_key "event_template_cuisine_types", "cuisine_types"
  add_foreign_key "event_template_cuisine_types", "event_templates"
  add_foreign_key "event_templates", "companies"
  add_foreign_key "event_templates", "users"
  add_foreign_key "event_templates", "venue_experiences"
  add_foreign_key "service_provider_galleries", "service_providers"
  add_foreign_key "service_provider_pictures", "service_provider_galleries"
  add_foreign_key "service_provider_review_comments", "service_provider_reviews"
  add_foreign_key "service_provider_review_comments", "users"
  add_foreign_key "service_provider_reviews", "service_providers"
  add_foreign_key "service_provider_reviews", "users"
  add_foreign_key "service_providers", "addresses"
  add_foreign_key "service_providers", "companies"
  add_foreign_key "service_providers", "service_provider_types"
  add_foreign_key "users", "account_types"
end
