require 'faker';

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Create Company Types
event_planner_agency = CompanyType
  .find_or_create_by!(
    name: :event_planning_agency,
    display_name: 'Event Planner Agency',
    description: 'An Agency that has one or more event planning companies'
  )
event_planner_company = CompanyType
  .find_or_create_by!(
    name: :event_planning_company,
    display_name: 'Event Planner Company',
    description: 'An event planning company'
  )
service_provider_agency = CompanyType
  .create_with(name: :service_provider_agency)
  .find_or_create_by(
    name: :service_provider_agency,
    display_name: 'Service Provider Agency',
    description: 'An Agency that has one or more service providers companies'
  )
service_provider_company = CompanyType
  .find_or_create_by(
    name: :service_provider_company,
    display_name: 'Service Provider Company',
    description: 'A service provider company'
  )

# Create Service Provider Types
catering_sp_type = ServiceProviderType
  .find_or_create_by(name: :catering, display_name: 'Catering', description: 'Catering Companies')
food_truck_sp_type = ServiceProviderType.find_or_create_by(name: :food_truck, display_name: 'Food Truck', description: 'Food Trucks')
restaurant_sp_type = ServiceProviderType.find_or_create_by(name: :restaurant, display_name: 'Restaurant', description: 'Restaurants')
venue_sp_type = ServiceProviderType.find_or_create_by(name: :venue, display_name: 'Venue', description: 'Venues')

# Create account Types
event_planner_account_type = AccountType
  .create_with(description: 'Event Planning Account Type')
  .find_or_create_by(name: :event_planner, display_name: 'Event Planner')
service_provider_account_type = AccountType
  .create_with(description: 'Service Provider Acount Type')
  .find_or_create_by(name: :service_provider, display_name: 'Vendor or Venue')

# Create Roles
owner_role = Role.create_with(description: 'Can read, recreate, update, and destroy', display_name: 'Owner')
  .find_or_create_by(name: :owner)
contributor_role = Role.create_with(description: 'Can contribute', display_name: 'Contributor')
  .find_or_create_by(name: :contributor)
reader_role = Role.create_with(description: 'Can read', display_name: 'Reader')
  .find_or_create_by(name: :reader)

if Rails.env.development? || Rails.env.production?

  # Create mock users
  chris_ep_user = User
    .create_with(
      first_name: 'Chris',
      last_name: 'Kittredge',
      password: 'password',
      password_confirmation: 'password',
      account_type: event_planner_account_type,
      is_admin: true
    )
    .find_or_create_by(email: 'chris.ep@randavoo.com')

  ogden_ep_user = User
    .create_with(
      first_name: 'Ogden',
      last_name: 'Hansford',
      password: 'password',
      password_confirmation: 'password',
      account_type: event_planner_account_type,
      is_admin: true
    )
    .find_or_create_by(email: 'ogden.ep@randavoo.com')

  chris_sp_user = User
    .create_with(
      first_name: 'Chris',
      last_name: 'Kittredge',
      password: 'password',
      password_confirmation: 'password',
      account_type: service_provider_account_type,
      is_admin: true,
    )
    .find_or_create_by(email: 'chris.sp@randavoo.com')

  ogden_sp_user = User
    .create_with(
      first_name: 'Ogden',
      last_name: 'Hansford',
      password: 'password',
      password_confirmation: 'password',
      account_type: service_provider_account_type,
      is_admin: true,
    )
    .find_or_create_by(email: 'ogden.sp@randavoo.com')

  # Create mock companies

  yumbii_addr = Address.
    find_or_create_by(
      street_address_1: '1927 Peachtree Rd NE',
      locality: 'Atlanta',
      region: 'GA',
      postcode: '30309',
      country: 'US',
      lonlat: RGeo::Geos.factory.point(-84.393467, 33.807377)
    )

  yumbii = Company
    .create_with(company_type: service_provider_company, address: yumbii_addr)
    .find_or_create_by(name: 'Yumbii')


  # Create mock memberships

  CompanyMembership.find_or_create_by(company: yumbii, user: chris_sp_user, role: owner_role)
  CompanyMembership.find_or_create_by(company: yumbii, user: ogden_sp_user, role: owner_role)

  # Create mock service providers

  yumbii_taco_shop = ServiceProvider
    .create_with(
      name: 'Yumbii Taco Shop',
      overview: 'Yumbii devotees can finally stop worrying about where to catch the truck, because we have a permanent home. Offering both counter service and seating for dine-in customers; we look forward to welcoming you to our first Yumbii restaurant.',
    )
    .find_or_create_by(company_id: yumbii.id, service_provider_type: restaurant_sp_type)

  yumbii_taco_shop_gallery = ServiceProviderGallery
    .find_or_create_by(name: :main_gallery, service_provider: yumbii_taco_shop)

  yumbii_taco_shop_gallery.service_provider_pictures.create(
    order: 1,
    image: File.new("#{Rails.root}/demo_account/service_provider_images/yumbii_taco_shop.jpg")
  )
  yumbii_taco_shop_gallery.service_provider_pictures.create(
    order: 2,
    image: File.new("#{Rails.root}/demo_account/service_provider_images/yumbii_sandwiches.jpg")
  )

  yumbii_food_truck = ServiceProvider
    .create_with(
      name: 'Yumbii Food Truck',
      overview: 'Yumbii is Atlanta’s first and finest food truck, offering an authentic, gourmet fusion of world flavors with a street food twist. Dubbed “Far Out Food” for its scrumptious blend of Asian and Mexican flavors, you can find Yumbii rolling around all over the city. From Alpharetta to Atlantic Station, Vinings to VAHI, Yumbii is the tastiest food truck grub east of the Mississippi. In addition to our daily lunch and dinner runs, you can catch the Yumbii trucks at the most popular events throughout ATL, including the Sweetwater 420 Festival, Candler Park Fall Fest, and more!',
    )
    .find_or_create_by(company_id: yumbii.id, service_provider_type: food_truck_sp_type)

  yumbii_food_truck_gallery = ServiceProviderGallery
    .find_or_create_by(name: :main_gallery, service_provider: yumbii_food_truck)

  yumbii_food_truck_gallery.service_provider_pictures.create(
    order: 1,
    image: File.new("#{Rails.root}/demo_account/service_provider_images/yumbii_food_truck.jpg")
  )
  yumbii_food_truck_gallery.service_provider_pictures.create(
    order: 2,
    image: File.new("#{Rails.root}/demo_account/service_provider_images/yumbii_sandwiches.jpg")
  )

  yumbii_catering = ServiceProvider
    .create_with(
      name: 'Yumbii Catering',
      overview: 'Yumbii offers a number of menu options for catering weddings, private parties, corporate lunches, and even movie sets.',
    )
    .find_or_create_by(company_id: yumbii.id, service_provider_type: catering_sp_type)

  yumbii_catering_gallery = ServiceProviderGallery
    .find_or_create_by(name: :main_gallery, service_provider: yumbii_catering)

  yumbii_catering_gallery.service_provider_pictures.create(
    order: 1,
    image: File.new("#{Rails.root}/demo_account/service_provider_images/yumbii_tacos.jpg")
  )
  yumbii_catering_gallery.service_provider_pictures.create(
    order: 2,
    image: File.new("#{Rails.root}/demo_account/service_provider_images/yumbii_sandwiches.jpg")
  )

  ServiceProviderReview
    .find_or_create_by(
      rating: 5,
      header: 'Great food and atmosphere!',
      text: 'This place has some great food and the atmosphere is incredible! Would definitely recommend this place to anyone looking for a new place to try!',
      service_provider: yumbii_taco_shop,
      user: chris_ep_user
    )
  ServiceProviderReview
    .find_or_create_by(
      rating: 4,
      header: 'Good food, but a small floorplan',
      text: 'Really good food, but not the most room to host a party bigger than 20 people.',
      service_provider: yumbii_taco_shop,
      user: ogden_ep_user
    )

  VenueExperience
    .find_or_create_by(
      name: 'casual',
      display_name: 'Casual',
      description: 'This place is casual'
    )

    VenueExperience
      .find_or_create_by(
        name: 'classy',
        display_name: 'Classy',
        description: 'This place is classy'
      )

    VenueExperience
      .find_or_create_by(
        name: 'exotic',
        display_name: 'Exotic',
        description: 'This place is exotic'
      )

    CuisineType
      .find_or_create_by(
        name: 'american',
        display_name: 'American',
        description: 'This food is American'
      )

    CuisineType
      .find_or_create_by(
        name: 'cuban',
        display_name: 'Cuban',
        description: 'This food is Cuban'
      )

    CuisineType
      .find_or_create_by(
        name: 'tex_mex',
        display_name: 'Tex-Mex',
        description: 'This food is Tex-Mex'
      )

end
