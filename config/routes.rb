require 'api_constraints'

Rails.application.routes.draw do
  devise_for :users, skip: [:sessions, :registrations]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # constraints: { subdomain: 'api' }
  namespace :api, defaults: { format: :json }, path: '/'  do
    scope module: :v1,
              constraints: ApiConstraints.new(version: 1, default: true) do
      resources :account_types, only: [:index]
      resources :users
      resources :registrations, path: '/register', only: [:create]
      resources :auth, only: [:show, :create, :destroy]
      resources :companies
      resources :company_memberships
      resources :company_types
      resources :roles

      resources :service_providers do
        resources :service_provider_galleries, path: '/galleries' do
          resources :service_provider_pictures, path: '/pictures', only: [:index, :show, :update, :destroy]
        end
        resources :service_provider_reviews, path: '/reviews' do
          resources :service_provider_review_comments, path: '/comments'
        end
      end

      resources :service_provider_types

      resources :event_templates
      resources :venue_experiences
      resources :cuisine_types

    end
  end

end
