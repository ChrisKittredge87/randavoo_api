class NewUserMailer < ApplicationMailer

  def welcome_email
    @user = User.find_by(email: 'DrumKitt87@gmail.com')
    @url = 'http://www.randavoo.com/confirm'
    mail(to: @user.email, subject: 'Confirm your registration to Randavoo!')
  end

end
