class CompanyPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      company_ids = user.company_memberships.pluck(:company_id)
      Company.where(id: company_ids)
    end
  end

  def show?
    user.company_memberships.exists?(company_id: record.id)
  end

  def create?
    cud_auth && match_account_type_and_company_type
  end

  def update?
    cud_auth && match_account_type_and_company_type
  end

  def destroy?
    cud_auth
  end

  private
  def cud_auth
    user.is_admin? &&
    (record.parent_id.nil? || user.company_memberships.pluck(:company_id).include?(record.parent_id))
  end

  def match_account_type_and_company_type
    company_type = CompanyType.find(record.company_type.id).try(:name).try(:to_sym)
    account_type = user.account_type.name.to_sym
    return true if [
      :event_planning_agency,
      :event_planning_company
    ].include?(company_type) && account_type == :event_planner
    return true if [
      :service_provider_agency,
      :service_provider_company
    ].include?(company_type.to_sym) && account_type == :service_provider
    false
  end
end
