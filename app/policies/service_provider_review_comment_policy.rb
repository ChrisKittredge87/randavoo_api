class ServiceProviderReviewCommentPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    can_cud?
  end

  def update?
    can_cud?
  end

  def destroy?
    can_cud?
  end

  private
  def can_cud?
    company_id = record.service_provider_review.service_provider.company_id
    user.has_company_membership?(company_id: company_id, role_id: Role.find_by(name: :owner).try(:id))
  end
end
