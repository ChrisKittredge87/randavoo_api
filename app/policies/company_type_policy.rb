class CompanyTypePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      user_account_type = user.account_type.name.to_sym
      if user_account_type == :event_planner
        scope.where(name: [:event_planning_agency, :event_planning_company])
      elsif user_account_type == :service_provider
        scope.where(name: [:service_provider_agency, :service_provider_company])
      else
        scope.where(id: -1)
      end
    end
  end

  def index?
    !user.nil?
  end
end
