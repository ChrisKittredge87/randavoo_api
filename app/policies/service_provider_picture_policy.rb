class ServiceProviderPicturePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    true
  end

  def show?
    true
  end

  def update?
    can_update_destroy?
  end

  def destroy?
    can_update_destroy?
  end

  private
  def can_update_destroy?
    company_id = record.service_provider_gallery.try(:service_provider).try(:company_id)
    return false if company_id.nil?
    user.has_company_membership?(company_id: company_id, role_id: Role.find_by(name: :owner).try(:id))
  end
end
