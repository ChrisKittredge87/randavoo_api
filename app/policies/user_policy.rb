class UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      user.is_admin? ? User.where(id: user.managed_user_ids << user.id) : User.where(id: user.id)
    end
  end

  def show?
    user.is_admin? && user.managed_user_ids.include?(record.id) || user.id == record.id
  end

  def create?
    user.is_admin?
  end

  def update?
    (user.is_admin? || user.id == record.id) &&
    record.id != record.manager_id &&
    (user.id == record.id || user.managed_user_ids.include?(record.id))
  end

  def destroy?
    (user.is_admin? && user.managed_user_ids.include?(record.id)) ||
    (user.is_admin? && user.managed_user_ids.count == 0 && user.id == record.id)
  end
end
