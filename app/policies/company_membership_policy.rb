class CompanyMembershipPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      valid_user_ids = user.managed_user_ids << user.id
      valid_company_ids = scope.joins(:role).where(user_id: user.id).where("roles.name = 'owner'").pluck(:company_id)
      scope.where(user_id: valid_user_ids, company_id: valid_company_ids)
    end
  end

  def index?
    user.is_admin?
  end

  def show?
    user.is_admin? && scope_ids.include?(record.id)
  end

  def create?
    (
      user.is_admin? &&
      user.manages_user?(record.user_id) &&
      user.has_company_membership?(company_id: record.company_id) &&
      check_account_type_company_type_validity(User.find(record.user_id), Company.find(record.company_id))
    )
  end

  def update?
    user.is_admin? && scope_ids.include?(record.id)
  end

  def destroy?
    user.is_admin? && scope_ids.include?(record.id)
  end

  private
  def check_account_type_company_type_validity(user, company)
    return false if user.nil? || company.nil?
    return true if user.account_type.name.to_sym == :event_planner && [:event_planning_agency, :event_planning_company].include?(company.company_type.name.to_sym)
    return true if user.account_type.name.to_sym == :service_provider && [:service_provider_agency, :service_provider_company].include?(company.company_type.name.to_sym)
    false
  end

  def scope_ids
    scope_ids = Scope.new(user, CompanyMembership).resolve.pluck(:id)
  end

end
