class ServiceProviderGalleryPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    record.can_edit?(user)
  end

  def update?
    record.can_edit?(user)
  end

  def destroy?
    record.can_edit?(user)
  end
end
