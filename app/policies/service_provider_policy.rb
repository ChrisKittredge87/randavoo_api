class ServiceProviderPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    (
      user.company_memberships
        .find_by(company_id: record.company_id)
        .try(:role).try(:name).try(:to_sym)
    ) == :owner
  end

  def update?
    record.can_edit?(user)
  end

  def destroy?
    user.is_admin? && record.can_edit?(user)
  end
end
