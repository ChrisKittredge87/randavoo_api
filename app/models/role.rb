# == Schema Information
#
# Table name: roles
#
#  id           :integer          not null, primary key
#  name         :string           not null
#  description  :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  display_name :string(100)
#
# Indexes
#
#  index_roles_on_name  (name) UNIQUE
#

class Role < ApplicationRecord
  validates :name, length: { maximum: 25 }, inclusion: {
    in: ['owner', 'contributor', 'reader'],
    message: "%{value} is not a valid"
  }, presence: true
  validates :display_name, length: { maximum: 100 }, presence: true
  validates :description, length: { maximum: 100 }, presence: true

  scope :owner, -> { find_by(name: :owner) }
  scope :contributor, -> { find_by(name: :contributor) }
  scope :reader, -> { find_by(name: :reader) }
end
