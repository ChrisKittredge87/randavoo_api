# == Schema Information
#
# Table name: companies
#
#  id              :integer          not null, primary key
#  name            :string(250)      not null
#  company_type_id :integer          not null
#  address_id      :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  parent_id       :integer
#
# Indexes
#
#  index_companies_on_address_id       (address_id)
#  index_companies_on_company_type_id  (company_type_id)
#  index_companies_on_name             (name)
#  index_companies_on_parent_id        (parent_id)
#
# Foreign Keys
#
#  fk_rails_...  (address_id => addresses.id)
#  fk_rails_...  (company_type_id => company_types.id)
#

class Company < ApplicationRecord
  belongs_to :company_type
  belongs_to :company, foreign_key: :parent_id, optional: true
  belongs_to :address, optional: true
  has_many :companies, foreign_key: :parent_id, dependent: :restrict_with_error
  has_many :service_providers, dependent: :restrict_with_error
  has_many :company_memberships, dependent: :destroy
  alias_attribute :parent, :company
  alias_attribute :children, :companies

  validates :name, length: { maximum: 250 }, presence: true

  def has_children?
    self.children.count > 0
  end

  def is_leaf?
    !has_children?
  end

  def can_edit?(user)
    return false if user.nil?
    user_company_membership_role(user) == :owner
  end

  def can_contribute?(user)
    return false if user.nil?
    can_edit?(user) || user_company_membership_role(user) == :contributor
  end

  private
  def user_company_membership_role(user)
    (
      user.company_memberships
        .find_by(company_id: self.id)
        .try(:role).try(:name).try(:to_sym)
    )
  end
end
