# == Schema Information
#
# Table name: account_types
#
#  id           :integer          not null, primary key
#  name         :string(50)       not null
#  description  :string(150)      not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  display_name :string(100)      not null
#

class AccountType < ApplicationRecord
  validates :name, length: { maximum: 50 }, presence: true
  validates :description, length: { maximum: 150 }, presence: true

  scope :event_planner, -> { find_by(name: :event_planner) }
  scope :service_provider, -> { find_by(name: :service_provider) }
end
