# == Schema Information
#
# Table name: addresses
#
#  id               :integer          not null, primary key
#  street_address_1 :string(200)      not null
#  street_address_2 :string(200)
#  locality         :string(200)      not null
#  region           :string(200)      not null
#  postcode         :string(100)      not null
#  country          :string(200)      not null
#  lonlat           :geography({:srid point, 4326
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_addresses_on_lonlat  (lonlat)
#

class Address < ApplicationRecord
  validates :street_address_1, length: { maximum: 200 }, presence: true
  validates :street_address_2, length: { maximum: 200 }, presence: false
  validates :locality, length: { maximum: 200 }, presence: true
  validates :region, length: { maximum: 200 }, presence: true
  validates :postcode, length: { maximum: 100 }, presence: true
  validates :country, length: { maximum: 200 }, presence: true
end
