# == Schema Information
#
# Table name: service_provider_pictures
#
#  id                          :integer          not null, primary key
#  name                        :string(100)
#  description                 :string(250)
#  image_file_name             :string           not null
#  image_content_type          :string           not null
#  image_file_size             :integer          not null
#  image_updated_at            :datetime         not null
#  service_provider_gallery_id :integer
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  order                       :integer          default(0), not null
#
# Indexes
#
#  index_service_provider_pictures_on_service_provider_gallery_id  (service_provider_gallery_id)
#
# Foreign Keys
#
#  fk_rails_...  (service_provider_gallery_id => service_provider_galleries.id)
#

class ServiceProviderPicture < ApplicationRecord
  belongs_to :service_provider_gallery

  validates :name, length: { maximum: 100 }
  validates :description, length: { maximum: 250 }

  has_attached_file :image, preserve_files: false

  do_not_validate_attachment_file_type :image

  Paperclip.interpolates :service_provider_id do |attachment, style|
    attachment.instance.service_provider_gallery.service_provider_id
  end

  Paperclip.interpolates :gallery_id do |attachment, style|
    attachment.instance.service_provider_gallery.id
  end
end
