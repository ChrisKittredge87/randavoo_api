# == Schema Information
#
# Table name: service_providers
#
#  id                       :integer          not null, primary key
#  name                     :string(250)
#  overview                 :string(5000)
#  travel_distance          :float
#  company_id               :integer          not null
#  service_provider_type_id :integer          not null
#  address_id               :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#
# Indexes
#
#  index_service_providers_on_address_id                (address_id)
#  index_service_providers_on_company_id                (company_id)
#  index_service_providers_on_service_provider_type_id  (service_provider_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (address_id => addresses.id)
#  fk_rails_...  (company_id => companies.id)
#  fk_rails_...  (service_provider_type_id => service_provider_types.id)
#

class ServiceProvider < ApplicationRecord
  belongs_to :company
  belongs_to :service_provider_type
  belongs_to :address, optional: true
  has_many :service_provider_galleries, dependent: :destroy
  has_many :service_provider_reviews

  validates :name, length: { maximum: 250 }
  validates :overview, length: { maximum: 5000 }

  def address
    self.address_id ? self.address : company.address
  end

  def can_edit?(user)
    self.company.can_edit?(user)
  end

  def can_contribute?(user)
    self.company.can_contribute?(user)
  end

  def average_rating
    ratings = self.service_provider_reviews.pluck(:rating)
    non_zero_divide(ratings.inject(&:+).to_f, ratings.count).round(1)
  end

  private
  def non_zero_divide(numerator, denominator)
    return 0 unless numerator > 0 && denominator > 0
    numerator / denominator
  end
end
