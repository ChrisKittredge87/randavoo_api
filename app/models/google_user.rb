# == Schema Information
#
# Table name: google_users
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  google_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_google_users_on_user_id  (user_id)
#

class GoogleUser < ApplicationRecord
end
