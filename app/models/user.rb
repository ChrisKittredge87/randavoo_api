# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  first_name             :string(250)      not null
#  last_name              :string(250)      not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  auth_token             :string           default("")
#  token_expiry           :datetime         default(Sun, 09 Jul 2017 21:24:30 UTC +00:00), not null
#  is_admin               :boolean          default(FALSE)
#  account_type_id        :integer          not null
#  deleted_at             :datetime
#  datetime               :datetime
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  manager_id             :integer
#
# Indexes
#
#  index_users_on_account_type_id       (account_type_id)
#  index_users_on_auth_token            (auth_token) UNIQUE
#  index_users_on_deleted_at            (deleted_at)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_manager_id            (manager_id)
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (account_type_id => account_types.id)
#

class User < ApplicationRecord
  acts_as_paranoid
  before_create :generate_authentication_token!
  belongs_to :account_type
  belongs_to :user, foreign_key: :manager_id, optional: true
  has_many :company_memberships, dependent: :destroy
  has_many :users, foreign_key: :manager_id
  alias_attribute :manager, :user
  alias_attribute :managed_users, :users

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

   validates :auth_token, uniqueness: true, allow_blank: true
   validates :token_expiry, presence: true
   validates :first_name, :last_name, length: { maximum: 250 }, presence: true

   def generate_authentication_token!
     begin
       self.auth_token = Devise.friendly_token
       self.token_expiry = Time.now + 1.day
     end while self.class.exists?(auth_token: auth_token)
     true
   end

   def has_company_membership?(company_id: nil, role_id: nil)
     return false if (company_id.nil? && role_id.nil?) || self.company_memberships.count == 0
     @company_memberships = self.company_memberships
     if (company_id && role_id)
       return !@company_memberships.find_by(company_id: company_id, role_id: role_id).nil?
     end
     @company_memberships = @company_memberships.find_by(company_id: company_id) if company_id
     @company_memberships = @company_memberships.find_by(role_id: role_id) if role_id
     !@company_memberships.nil?
   end

   # Return true if user with user_id is managed by this user
   def manages_user?(user_id)
     return false if self.managed_users.count == 0
     !self.managed_users.find_by(id: user_id).nil?
   end

   def managed_user_ids
     recurse_managed_users(self.managed_users, [])
   end

   private
   def recurse_managed_users(users, ids)
     ids.concat(users.pluck(:id)).concat(users.map { |u| recurse_managed_users u.managed_users, [] }.flatten)
   end
end
