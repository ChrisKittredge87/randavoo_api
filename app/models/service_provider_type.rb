# == Schema Information
#
# Table name: service_provider_types
#
#  id           :integer          not null, primary key
#  name         :string(250)      not null
#  description  :string(500)      not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  display_name :string           not null
#

class ServiceProviderType < ApplicationRecord
  validates :name, length: { maximum: 250 }, presence: true
  validates :description, length: { maximum: 500 }, presence: true
  validates :display_name, length: { maximum: 100 }, presence: true
end
