# == Schema Information
#
# Table name: service_provider_galleries
#
#  id                  :integer          not null, primary key
#  name                :string(100)      not null
#  description         :string(250)
#  service_provider_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_service_provider_galleries_on_service_provider_id  (service_provider_id)
#
# Foreign Keys
#
#  fk_rails_...  (service_provider_id => service_providers.id)
#

class ServiceProviderGallery < ApplicationRecord
  belongs_to :service_provider
  has_many :service_provider_pictures, dependent: :destroy

  validates :name, length: { maximum: 100 }, presence: true
  validates :description, length: { maximum: 250 }

  def can_edit?(user = nil)
    return false if user.nil?
    if company = Company.find(self.service_provider.company_id)
      return company.can_edit?(user)
    end
    false
  end
end
