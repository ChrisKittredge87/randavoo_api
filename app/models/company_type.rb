# == Schema Information
#
# Table name: company_types
#
#  id           :integer          not null, primary key
#  name         :string(50)       not null
#  description  :string(150)      not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  display_name :string(100)
#

class CompanyType < ApplicationRecord
  validates :name, length: { maximum: 50 }, presence: true
  validates :display_name, length: { maximum: 100 }, presence: true
  validates :description, length: { maximum: 150 }, presence: true

  scope :event_planning_agency, -> { find_by(name: :event_planning_agency) }
  scope :event_planning_company, -> { find_by(name: :event_planning_company) }
  scope :service_provider_agency, -> { find_by(name: :service_provider_agency) }
  scope :service_provider_company, -> { find_by(name: :service_provider_company) }
end
