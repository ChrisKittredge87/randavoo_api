# == Schema Information
#
# Table name: company_memberships
#
#  id         :integer          not null, primary key
#  role_id    :integer          not null
#  company_id :integer          not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_company_memberships_on_company_id              (company_id)
#  index_company_memberships_on_role_id                 (role_id)
#  index_company_memberships_on_user_id                 (user_id)
#  index_company_memberships_on_user_id_and_company_id  (user_id,company_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (company_id => companies.id)
#  fk_rails_...  (role_id => roles.id)
#  fk_rails_...  (user_id => users.id)
#

class CompanyMembership < ApplicationRecord
  belongs_to :company
  belongs_to :user
  belongs_to :role

  validates :user, uniqueness: { scope: :company }

  def can_edit?(user)
    self.user != user
  end
end
