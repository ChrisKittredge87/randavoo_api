# == Schema Information
#
# Table name: venue_experiences
#
#  id           :integer          not null, primary key
#  description  :string(500)
#  name         :string(100)      not null
#  display_name :string(100)      not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class VenueExperienceSerializer < ActiveModel::Serializer
  attributes :id, :name, :display_name, :description
end
