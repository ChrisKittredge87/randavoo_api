# == Schema Information
#
# Table name: service_providers
#
#  id                       :integer          not null, primary key
#  name                     :string(250)
#  overview                 :string(5000)
#  company_id               :integer
#  service_provider_type_id :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#
# Indexes
#
#  index_service_providers_on_company_id                (company_id)
#  index_service_providers_on_service_provider_type_id  (service_provider_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (company_id => companies.id)
#  fk_rails_...  (service_provider_type_id => service_provider_types.id)
#

class ServiceProviderSimpleSerializer < ActiveModel::Serializer
  belongs_to :company, serializer: CompanyNoChildrenSerializer

  attributes :id, :name, :service_provider_type, :can_contribute, :can_edit

  def service_provider_type
    ServiceProviderTypeSerializer.new(object.service_provider_type, root: false)
  end

  def can_edit
    object.can_edit?(instance_options[:scope])
  end

  def can_contribute
    object.can_contribute?(instance_options[:scope])
  end
end
