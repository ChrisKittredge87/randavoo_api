# == Schema Information
#
# Table name: event_templates
#
#  id                          :integer          not null, primary key
#  user_id                     :integer          not null
#  company_id                  :integer          not null
#  dont_set_budget             :boolean          not null
#  start_date                  :datetime         not null
#  end_date                    :datetime         not null
#  number_of_expected_guests   :integer          not null
#  overall_budget              :float
#  lonlat                      :geography({:srid not null, point, 4326
#  address                     :string           not null
#  include_in_house_vendors    :boolean          not null
#  include_outside_vendors     :boolean          not null
#  needs_venue                 :boolean          not null
#  radius                      :integer
#  venue_budget                :float
#  venue_experience_id         :integer
#  vendor_budget               :float            not null
#  caterer_quantity            :integer          not null
#  budget_per_caterer          :float
#  food_truck_quantity         :integer          not null
#  budget_per_food_truck       :float
#  food_truck_paid_for_by_host :boolean
#  description                 :string(500)      not null
#  name                        :string(100)      not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_event_templates_on_company_id           (company_id)
#  index_event_templates_on_lonlat               (lonlat)
#  index_event_templates_on_user_id              (user_id)
#  index_event_templates_on_venue_experience_id  (venue_experience_id)
#
# Foreign Keys
#
#  fk_rails_...  (company_id => companies.id)
#  fk_rails_...  (user_id => users.id)
#  fk_rails_...  (venue_experience_id => venue_experiences.id)
#

class EventTemplateSerializer < ActiveModel::Serializer
  attributes(
    :id,
    :user_id,
    :company_id,
    :dont_set_budget,
    :start_date,
    :end_date,
    :number_of_expected_guests,
    :overall_budget,
    :address,
    :lat,
    :lon,
    :include_in_house_vendors,
    :include_outside_vendors,
    :needs_venue,
    :radius,
    :venue_budget,
    :venue_experience_id,
    :vendor_budget,
    :caterer_quantity,
    :budget_per_caterer,
    :food_truck_quantity,
    :budget_per_food_truck,
    :food_truck_paid_for_by_host,
    :name,
    :description,
    :cuisine_type_ids
  )

  def cuisine_type_ids
    object.event_template_cuisine_types.map do |cuisine_type|
      cuisine_type[:id]
    end
  end

  def lat
    object[:lonlat].lat
  end

  def lon
    object[:lonlat].lon
  end
end
