# == Schema Information
#
# Table name: cuisine_types
#
#  id           :integer          not null, primary key
#  description  :string(500)      not null
#  name         :string(100)      not null
#  display_name :string(100)      not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class CuisineTypeSerializer < ActiveModel::Serializer
  attributes :id, :name, :display_name, :description
end
