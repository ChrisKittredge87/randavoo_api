# == Schema Information
#
# Table name: companies
#
#  id              :integer          not null, primary key
#  name            :string           not null
#  company_type_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  parent_id       :integer
#
# Indexes
#
#  index_companies_on_company_type_id  (company_type_id)
#  index_companies_on_parent_id        (parent_id)
#
# Foreign Keys
#
#  fk_rails_...  (company_type_id => company_types.id)
#
class CompanyNoChildrenSerializer < ActiveModel::Serializer
  belongs_to :company_type
  has_many :service_providers, serializer: ServiceProviderSimpleSerializer

  attributes :id, :name, :company_type, :service_providers, :can_contribute, :can_edit

  def can_contribute
    object.can_contribute?(instance_options[:scope])
  end

  def can_edit
    object.can_edit?(instance_options[:scope])
  end
end
