# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  first_name             :string(250)      not null
#  last_name              :string(250)      not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  auth_token             :string           default("")
#  token_expiry           :datetime         default(Sun, 09 Jul 2017 21:24:30 UTC +00:00), not null
#  is_admin               :boolean          default(FALSE)
#  account_type_id        :integer          not null
#  deleted_at             :datetime
#  datetime               :datetime
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  manager_id             :integer
#
# Indexes
#
#  index_users_on_account_type_id       (account_type_id)
#  index_users_on_auth_token            (auth_token) UNIQUE
#  index_users_on_deleted_at            (deleted_at)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_manager_id            (manager_id)
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (account_type_id => account_types.id)
#

class SessionUserSerializer < ActiveModel::Serializer
  belongs_to :account_type
  attributes :id, :email, :is_admin, :auth_token, :account_type
end
