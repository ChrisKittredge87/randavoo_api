# == Schema Information
#
# Table name: roles
#
#  id           :integer          not null, primary key
#  name         :string           not null
#  description  :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  display_name :string(100)
#
# Indexes
#
#  index_roles_on_name  (name) UNIQUE
#

class RoleSerializer < ActiveModel::Serializer
  attributes :id, :name, :display_name, :description
end
