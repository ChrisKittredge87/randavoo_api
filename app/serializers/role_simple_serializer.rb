class RoleSimpleSerializer < ActiveModel::Serializer
  attributes :id, :name
end
