# == Schema Information
#
# Table name: service_provider_galleries
#
#  id                  :integer          not null, primary key
#  name                :string(100)      not null
#  description         :string(250)
#  service_provider_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_service_provider_galleries_on_service_provider_id  (service_provider_id)
#
# Foreign Keys
#
#  fk_rails_...  (service_provider_id => service_providers.id)
#

class ServiceProviderGallerySerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :pictures

  def pictures
    ActiveModelSerializers::SerializableResource.new(
      object.service_provider_pictures.order(:order),
      each_serializer: ServiceProviderPictureSerializer
    )
  end
end
