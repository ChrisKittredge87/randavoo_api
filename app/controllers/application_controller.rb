class ApplicationController < ActionController::API
  include Authenticable
  include ExceptionHandler
  include Pundit
  include Response

  respond_to :json
  before_action :transform_keys
  after_action :verify_authorized, except: :index unless Rails.env.test?
  after_action :verify_policy_scoped, only: :index unless Rails.env.test?

  rescue_from Pundit::NotAuthorizedError, with: :pundit_not_authorized

  private
  def pundit_not_authorized
    if request.method == 'GET'
      render json: { errors: 'Not found' }, status: :not_found
    else
      render json: { errors: 'Not authorized' }, status: 401
    end
  end

  def transform_keys
    params.transform_keys!(&:underscore)
  end

end
