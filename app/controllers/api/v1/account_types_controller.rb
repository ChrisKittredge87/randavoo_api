class Api::V1::AccountTypesController < ApplicationController

  def index
    render json: policy_scope(AccountType)
  end

end
