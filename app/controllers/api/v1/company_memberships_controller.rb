class Api::V1::CompanyMembershipsController < ApplicationController
  before_action :authenticate_with_token!
  before_action :set_company_membership, only: [:show, :update, :destroy]

  def index
    authorize CompanyMembership
    render json: filter_company_memberships
  end

  def show
    render json: @company_membership
  end

  def create
    @company_membership = CompanyMembership.new(company_membership_params)
    authorize @company_membership
    if @company_membership.save
      render json: @company_membership
    else
      render json: { errors: @company_membership.errors }, status: :unprocessable_entity
    end
  end

  def update
    @company_membership.update_attributes(company_membership_params)
    authorize @company_membership
    if @company_membership.save
      render json: @company_membership
    else
      render json: { errors: @company_membership.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @company_membership.destroy
      render json: { message: 'Deleted successfully!' }
    else
      render json: { errors: @company_membership.errors }, status: :unprocessable_entity
    end
  end

  private
  def company_membership_params
    params.require(:company_membership).permit(:company_id, :user_id, :role_id)
  end

  def filter_company_memberships
    @company_memberships = policy_scope(CompanyMembership)
    @company_memberships = @company_memberships.where(company_id: params[:company_id]) if params[:company_id]
    @company_memberships = @company_memberships.where(user_id: params[:user_id]) if params[:user_id]
    @company_memberships = @company_memberships.where(role_id: params[:role_id]) if params[:role_id]
    @company_memberships
  end

  def set_company_membership
    @company_membership = CompanyMembership.find(params[:id])
    authorize @company_membership
  end
end
