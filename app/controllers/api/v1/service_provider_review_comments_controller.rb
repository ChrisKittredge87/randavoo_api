class Api::V1::ServiceProviderReviewCommentsController < ApplicationController
  respond_to :json
  before_action only: [:index, :show] do
    authenticate_with_token!(true)
  end
  before_action :authenticate_with_token!, only: [:show, :update, :destroy]
  before_action :set_review_comment, only: [:show]

  def index
    render json: policy_scope(ServiceProviderReviewComment)
  end

  def show
    if @review_comment
      render json: @review_comment
    else
      head 404
    end
  end

  def create
    review_comment = ServiceProviderReviewComment.new(review_comment_params)
    authorize review_comment
    if review_comment.save
      render json: review_comment
    else
      render json: { errors: review_comment.errors }, status: :unprocessable_entity
    end
  end

  def update
    @review_comment.update_attributes(rewiew_comment_params)
    authorize review_comment
    if @review_comment.save
      render json: @review_comment
    else
      render json: { errors: @review_comment.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @review_comment.destroy
      head 204
    else
      render json: { errors: @review_comment.errors }, status: :unprocessable_entity
    end
  end

  private
  def review_comment_params
    params.require(:service_provider_review_comment).permit(:text)
  end

  def set_review_comment
    @review_comment = ServiceProviderReviewComment.find_by(id: params[:id], service_provider_review_id: params[:service_provider_review_id])
    authorize @review_comment || ServiceProviderReviewComment
  end
end
