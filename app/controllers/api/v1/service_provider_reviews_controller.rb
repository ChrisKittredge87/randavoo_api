class Api::V1::ServiceProviderReviewsController < ApplicationController
  respond_to :json
  before_action only: [:index, :show] do
    authenticate_with_token!(true)
  end
  before_action :set_review, only: [:show]

  def index
    render json: filter_reviews
  end

  def show
    if @review
      render json: @review
    else
      head 404
    end
  end

  private
  def filter_reviews
    reviews = policy_scope(ServiceProviderReview).where(service_provider_id: params[:service_provider_id])
    if params[:query]
      query = "%#{params[:query]}%"
      reviews = reviews.where('header ILIKE ?', query).or(reviews.where('text ILIKE ?', query))
    end
    if minimum_rating = params[:minimum_rating].to_i
      reviews = reviews.where('rating >= ?', minimum_rating)
    end
    reviews
  end

  def set_review
    @review = ServiceProviderReview.find_by(id: params[:id], service_provider_id: params[:service_provider_id])
    authorize @review || ServiceProviderReview
  end
end
