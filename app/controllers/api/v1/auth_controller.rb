class Api::V1::AuthController < ApplicationController
  skip_after_action :verify_authorized, except: :index unless Rails.env.test?

  def show
    if current_user
      render json: current_user, serializer: SessionUserSerializer
    else
      head 404
    end
  end

  def create
    email = params[:auth][:email]
    password = params[:auth][:password]
    user = User.find_by_email(email)
    if user && user.valid_password?(password)
      sign_in user, store: false
      if user.generate_authentication_token! && user.save
        render json: user, status: status, serializer: SessionUserSerializer
      else
        render json: { errors: "Something went wrong" }, status: :unprocessable_entity
      end
    else
      render json: { errors: "Invalid email or password" }, status: :unprocessable_entity
    end
  end

  def destroy
    user = User.find_by_auth_token(params[:id])
    if user.generate_authentication_token! && user.save
      head 204
    else
      render json: { errors: "Error destroying session" }, status: :unprocessable_entity
    end
  end

end
