class Api::V1::ServiceProviderTypesController < ApplicationController

  def index
    render json: policy_scope(ServiceProviderType)
  end

end
