class Api::V1::ServiceProviderGalleriesController < ApplicationController
  respond_to :json
  before_action only: [:index, :show] do
    authenticate_with_token!(true)
  end
  before_action :authenticate_with_token!, only: [:create, :update, :destroy]
  before_action :set_gallery, only: [:show, :update, :destroy]

  def index
    render json: filter_galleries, each_serializer: ServiceProviderGallerySerializer
  end

  def show
    render json: @gallery, serializer: ServiceProviderGallerySerializer
  end

  def create
    @gallery = ServiceProviderGallery.new(create_gallery_params)
    authorize @gallery
    if @gallery.save && create_images
      render json: @gallery, serializer: ServiceProviderGallerySerializer, status: :created
    else
      @gallery.destroy
      render json: @gallery.errors, status: :unprocessable_entity
    end
  end

  def update
    @gallery.update_attributes(update_gallery_params)
    authorize @gallery
    if @gallery.save && create_images
      render json: @gallery, serializer: ServiceProviderGallerySerializer, status: :updated
    else
      render json: @gallery.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @gallery.destroy
      head 204
    else
      render json: { errors: @gallery.errors }, status: :unprocessable_entity
    end
  end

  private
  def create_images
    if params[:images]
      params[:images].each { |image|
        @gallery.service_provider_pictures.create(image: image)
      }
    end
    true
  end

  def filter_galleries
    @galleries = policy_scope(ServiceProviderGallery)
    @galleries = @galleries.where(service_provider_id: params[:service_provider_id]) if params[:service_provider_id]
    @galleries
  end

  def create_gallery_params
    params.permit(:name, :description, :service_provider_id)
  end

  def update_gallery_params
    params.permit(:name, :description)
  end

  def set_gallery
    @gallery = ServiceProviderGallery.find(params[:id])
    authorize @gallery
  end

end
