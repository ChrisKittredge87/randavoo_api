class Api::V1::ServiceProviderPicturesController < ApplicationController
  respond_to :json
  before_action only: [:index, :show] do
    authenticate_with_token!(true)
  end
  before_action :authenticate_with_token!, only: [:update, :destroy]
  before_action :set_picture, only: [:show, :update, :destroy]

  def index
    render json: filter_pictures
  end

  def show
    if !@picture.nil?
      render json: @picture, status: status
    else
      head 404
    end
  end

  def update
    @picture.update_attributes(picture_params)
    authorize @picture
    if @picture.save
      render json: @picture
    else
      render json: { errors: @picture.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @picture.destroy
      head 204
    else
      render json: { errors: @picture.errors }, status: :unprocessable_entity
    end
  end

  private
  def filter_pictures
    @pictures = policy_scope(ServiceProviderPicture).where(service_provider_gallery_id: params[:service_provider_gallery_id])
    if params[:query]
      query = "%#{params[:query]}%"
      @pictures = @pictures.where('name ILIKE ?', query)
    end
    @pictures
  end

  def picture_params
    params.require(:service_provider_picture).permit(:name, :description, :service_provider_gallery_id)
  end

  def set_picture
    @picture = ServiceProviderPicture.find_by(id: params[:id], service_provider_gallery_id: params[:service_provider_gallery_id])
    authorize @picture || ServiceProviderPicture
  end

end
