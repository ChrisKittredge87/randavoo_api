class Api::V1::RegistrationsController < ActionController::API

  def create
    user = User.new(registration_params)
    if user.account_type == AccountType.event_planner
      company = Company
        .new(
          name: "#{user.last_name} Event Planning",
          company_type: CompanyType.event_planning_company
        )
    elsif user.account_type == AccountType.service_provider
      company = Company
        .new(
          name: "#{user.last_name} Services",
          company_type: CompanyType.service_provider_company
        )
    end
    company_membership = CompanyMembership.new(
      company: company,
      user: user,
      role: Role.owner,
    )
    if (user.generate_authentication_token! && user.save &&
      company.save && user.company_memberships && company_membership.save)
      render json: user, serializer: SessionUserSerializer
    else
      render json: { errors: user.errors }, status: :unprocessable_entity
    end
  end

  private
  def registration_params
    params
      .require(:user)
      .permit(:email, :first_name, :last_name, :password, :password_confirmation, :account_type_id)
      .merge!(is_admin: true)
  end
end
