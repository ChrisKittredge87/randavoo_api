class Api::V1::VenueExperiencesController < ApplicationController
  before_action :authenticate_with_token!

  def index
    render json: policy_scope(VenueExperience)
  end
end
