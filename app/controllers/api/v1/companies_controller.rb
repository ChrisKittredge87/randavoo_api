class Api::V1::CompaniesController < ApplicationController
  before_action :authenticate_with_token!
  before_action :set_company, only: [:show, :update, :destroy]

  def index
    render json: filter_companies.order(:name), each_serializer: CompanySerializer
  end

  def show
    render json: @company, serializer: CompanySerializer
  end

  def create
    @company = Company.new(company_params)
    authorize @company
    if @company.save && CompanyMembership.create(user: current_user, company: @company, role: Role.find_by(name: :owner))
      render json: @company, status: :created
    else
      @company.try(:destroy)
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  def update
    if @company.update(company_params)
      render json: @company, status: :created
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @company.children.count > 0
      render json: { errors: 'Company has child companies!' }, status: :unprocessable_entity
    elsif @company.destroy
      render json: { message: 'Successful delete' }
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  private
  def filter_companies
    @companies = policy_scope(Company)
    @companies = @companies.where('name ILIKE ?', "%#{params[:query]}%") if params[:query]
    @companies = @companies.where(company_type_id: params[:company_type_id]) if params[:company_type_id]
    @companies
  end

  def company_params
    params.require(:company).permit(:name, :company_type_id, :parent_id)
  end

  def set_company
    @company = Company.find(params[:id])
    authorize @company
  end

end
