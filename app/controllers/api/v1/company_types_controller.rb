class Api::V1::CompanyTypesController < ApplicationController
  before_action :authenticate_with_token!

  def index
    render json: policy_scope(CompanyType)
  end
end
