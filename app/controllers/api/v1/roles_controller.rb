class Api::V1::RolesController < ApplicationController
  before_action :authenticate_with_token!

  def index
    render json: policy_scope(Role)
  end
end
