class Api::V1::ServiceProvidersController < ApplicationController
  respond_to :json
  before_action only: [:index, :show] do
    authenticate_with_token!(true)
  end
  before_action :authenticate_with_token!, only: [:create, :update, :destroy]
  before_action :set_service_provider, only: [:show, :update, :destroy]

  def index
    render json: filter_service_providers, each_serializer: ServiceProviderSerializer
  end

  def show
    render json: @service_provider, serializer: ServiceProviderSerializer
  end

  def create
    @service_provider = ServiceProvider.new(create_service_provider_params)
    authorize @service_provider
    if @service_provider.save
      render json: @service_provider, status: :created
    else
      render json: { errors: @service_provider.errors }, status: :unprocessable_entity
    end
  end

  def update
    @service_provider.update_attributes(update_service_provider_params)
    authorize @service_provider
    if @service_provider.save
      render json: @service_provider, serializer: ServiceProviderSerializer
    else
      render json: { errors: @service_provider.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @service_provider.destroy
      head 204
    else
      render json: { errors: @service_provider.errors }, status: :unprocessable_entity
    end
  end

  private
  ORDER_MAPPINGS = {
    :average_rating_asc => "GREATEST(AVG(rating), 1.0) ASC",
    :average_rating_desc => "GREATEST(AVG(rating), 1.0) DESC",
  }

  def filter_service_providers
    @service_providers = policy_scope(ServiceProvider)
    @service_providers = @service_providers
      .joins(company: :company_memberships)
      .where('company_memberships.user_id = ? AND company_memberships.role_id = ?', current_user.id, Role.find_by(name: :owner)) if current_user && params[:managed] == 'true'
    @service_providers = @service_providers.where('name ILIKE ?', "%#{params[:query]}%") if params[:query]
    @service_providers = @service_providers.where(service_provider_type_id: params[:service_provider_type_id]) if params[:service_provider_type_id]
    @service_providers = @service_providers.where(company_id: params[:company_id]) if params[:company_id]
    @service_providers = @service_providers
      .joins("LEFT JOIN service_provider_reviews ON service_provider_reviews.service_provider_id = service_providers.id")
      .order(params[:sort_dir].try(:downcase).try(:strip) == 'asc' ? ORDER_MAPPINGS[:average_rating_asc] : ORDER_MAPPINGS[:average_rating_desc])
      .group("service_providers.id") if params[:sort].try(:include?, 'average_rating')
    @service_providers.order(:service_provider_type_id)
  end

  def set_service_provider
    @service_provider = ServiceProvider.find(params[:id])
    authorize @service_provider
  end

  def create_service_provider_params
    params.require(:service_provider).permit(:company_id, :service_provider_type_id, :name, :overview)
  end

  def update_service_provider_params
    params.require(:service_provider).permit(:name, :overview)
  end

end
