class Api::V1::ServiceProviderImagesController < ApplicationController
  respond_to :json
  before_action :authenticate_with_token!, only: [:show, :destroy]
  before_action :set_service_provider_image, only: [:show, :destroy]

  ACCESS_KEY_ID = 'AKIAJ7POJOU3AT63NC4A'
  SECRET_ACCESS_KEY = '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'

  def index
    json_response ServiceProviderImage.all
  end

  def show
    if !@service_provider_image.nil?
      json_response @service_provider_image
    else
      head 404
    end
  end

  def create
    s3 = Aws::S3::Resource.new(
      credentials: Aws::Credentials.new(ACCESS_KEY_ID, SECRET_ACCESS_KEY),
      region: 'us-east-1'
    )
    bucket_name = "service-provider-photos"
    obj = s3.bucket(bucket_name).object(params[:image].original_filename)
    obj.upload_file(params[:image].tempfile, acl:'public-read')

    image = ServiceProviderImage.new(url: obj.public_url, service_provider_id: params[:service_provider_id])
    if image.save
      json_response image, 201
    else
      render json: { errors: image.errors }, status: 422
    end
  end

  def destroy
    if @service_provider_image.nil?
      head 404
    else
      if @service_provider_image.destroy
        head 204
      else
        render json: { errors: @service_provider_image.errors }, status: 422
      end
    end
  end

  private
  def set_service_provider_image
    service_provider_image = ServiceProviderImage.find(params[:id])
    @service_provider_image = service_provider_image if service_provider_image.service_provider.company.company_memberships.pluck(:user_id).include? current_user.id
  end

  def service_provider_image_params
    params.require(:service_provider_image).permit(:url)
  end

end
