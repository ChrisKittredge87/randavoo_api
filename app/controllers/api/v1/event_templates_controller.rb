class Api::V1::EventTemplatesController < ApplicationController
  respond_to :json
  before_action only: [:index, :show] do
    authenticate_with_token!(true)
  end
  before_action :authenticate_with_token!, only: [:create, :update, :destroy]
  before_action :set_event_template, only: [:show, :update, :destroy]

  def index
    render json: policy_scope(EventTemplate), each_serializer: EventTemplateSerializer
  end

  def show
    render json: @event_template, serializer: EventTemplateSerializer
  end

  def create
    @event_template = EventTemplate.new(
      create_event_template_params.merge(
        lonlat: RGeo::Geos.factory
          .point(params[:event_template][:lon], params[:event_template][:lat])
      )
    )
    authorize @event_template
    if @event_template.save
      params[:event_template][:cuisine_type_ids].each do |cuisine_type_id|
        EventTemplateCuisineType
          .create(event_template: @event_template, cuisine_type_id: cuisine_type_id)
      end
      render json: @event_template, status: :created
    else
      render json: { errors: @event_template.errors }, status: :unprocessable_entity
    end
  end

  def update
    @event_template.update_attributes(update_event_template_params)
    authorize @event_template
    if @event_template.save
      render json: @event_template, serializer: EventTemplateSerializer
    else
      render json: { errors: @event_template.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @event_template.destroy
      head 204
    else
      render json: { errors: @event_template.errors }, status: :unprocessable_entity
    end
  end

  private
  def set_event_template
    @event_template = EventTemplate.find(params[:id])
    authorize @event_template
  end

  def create_event_template_params
    params
      .require(:event_template)
      .permit(:user_id,
        :company_id,
        :dont_set_budget,
        :start_date,
        :end_date,
        :number_of_expected_guests,
        :overall_budget,
        :address,
        :include_in_house_vendors,
        :include_outside_vendors,
        :needs_venue,
        :radius,
        :venue_budget,
        :venue_experience_id,
        :vendor_budget,
        :caterer_quantity,
        :budget_per_caterer,
        :food_truck_quantity,
        :budget_per_food_truck,
        :food_truck_paid_for_by_host,
        :name,
        :description
      )
  end

  def update_service_provider_params
    params
      .require(:event_template)
      .permit(:user_id,
        :company_id,
        :dont_set_budget,
        :start_date,
        :end_date,
        :number_of_expected_guests,
        :overall_budget,
        :address,
        :include_in_house_vendors,
        :include_outside_vendors,
        :needs_venue,
        :radius,
        :venue_budget,
        :venue_experience_id,
        :vendor_budget,
        :caterer_quantity,
        :budget_per_caterer,
        :food_truck_quantity,
        :budget_per_food_truck,
        :food_truck_paid_for_by_host,
        :name,
        :description
      )
  end
end
