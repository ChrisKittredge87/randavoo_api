class Api::V1::UsersController < ApplicationController
  respond_to :json
  before_action :authenticate_with_token!
  before_action :set_user, only: [:show, :update, :destroy]

  def index
    render json: filter_users.order(:last_name)
  end

  def show
    if !@user.nil?
      render json: @user, status: status
    else
      head 404
    end
  end

  def create
    @user = User.new(create_user_params)
    authorize @user
    if @user.save
      render json: @user, status: :created
    else
      render json: { errors: @user.errors }, status: :unprocessable_entity
    end
  end

  def update
    @user.update_attributes(update_user_params)
    authorize @user
    if @user.save
      json_response @user
    else
      render json: { errors: @user.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    if @user.destroy
      render json: { message: 'User deleted successfully' }
    else
      render json: { errors: @user.errors }, status: :unprocessable_entity
    end
  end

  private
  def filter_users
    @users = policy_scope(User)
    if params[:query]
      query = "%#{params[:query]}%"
      @users = @users
        .where('email ILIKE ?', query)
        .or(@users.where('first_name ILIKE ?', query))
        .or(@users.where('last_name ILIKE ?', query))
    end
    @users
  end

  def set_user
    @user = User.find(params[:id])
    authorize @user
  end

  def create_user_params
    temp_password = Devise.friendly_token.first(8)
    params
      .require(:user)
      .permit(:first_name, :last_name, :email, :is_admin)
      .merge!(password: temp_password, account_type_id: current_user.account_type_id, manager_id: params.dig(:user, :manager_id) || current_user.id)
  end

  def update_user_params
    if current_user.is_admin?
      params.require(:user).permit(:first_name, :last_name, :email, :is_admin, :manager_id)
    else
      params.require(:user).permit(:first_name, :last_name, :email)
    end
  end

end
