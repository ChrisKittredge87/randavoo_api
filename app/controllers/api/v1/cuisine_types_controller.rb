class Api::V1::CuisineTypesController < ApplicationController
  before_action :authenticate_with_token!

  def index
    render json: policy_scope(CuisineType)
  end
end
