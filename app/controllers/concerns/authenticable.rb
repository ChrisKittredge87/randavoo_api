module Authenticable

  def current_user
    @current_user ||= User.find_by_auth_token(request.headers["Authorization"])
  end

  def authenticate_with_token!(public_access = false)
    render json: { errors: "Not authenticated" }, status: :unauthorized unless user_signed_in? || public_access
  end

  # Check that the user is logged in and that the user auth token has not expired
  def user_signed_in?
    current_user.present? && current_user.token_expiry > Time.now
  end

end
