module Response
  def json_response(object, status = :ok, *args)
    render *args, json: object, status: status
  end
end
