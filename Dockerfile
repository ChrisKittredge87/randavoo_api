FROM heroku/cedar

RUN cd /tmp && git clone https://github.com/heroku/heroku-buildpack-ruby
ENV HOME=/app
WORKDIR /app

ENV CURL_CONNECT_TIMEOUT=0 CURL_TIMEOUT=0 GEM_PATH="$HOME/vendor/bundle/ruby/2.2.0:$GEM_PATH" LANG=${LANG:-en_US.UTF-8} PATH="$HOME/bin:$HOME/vendor/bundle/bin:$HOME/vendor/bundle/ruby/2.2.0/bin:$PATH" RACK_ENV=${RACK_ENV:-production} RAILS_ENV=${RAILS_ENV:-production} RAILS_LOG_TO_STDOUT=${RAILS_LOG_TO_STDOUT:-enabled} RAILS_SERVE_STATIC_FILES=${RAILS_SERVE_STATIC_FILES:-enabled} SECRET_KEY_BASE=${SECRET_KEY_BASE:-7170d9502c771d5a667b94d1bb27c4fff1449668bc4070a9b52c849ea379e480c7da4d5d6bfe4c6b1e39a6e73fa1ae2125aad7b6de835f752e70b3d187f15eee} STACK=cedar-14 

ARG BUNDLE_WITHOUT=development:test

# This is to install sqlite for any ruby apps that need it
# This line can be removed if your app doesn't use sqlite3
RUN apt-get update && apt-get install sqlite3 libsqlite3-dev && apt-get clean

COPY . /app

RUN output=$(/tmp/heroku-buildpack-ruby/bin/compile /app /tmp/cache) || echo $output
