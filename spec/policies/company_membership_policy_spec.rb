require 'rails_helper'

RSpec.describe CompanyMembershipPolicy do

  let(:event_planner_account_type) { create(:account_type, name: :event_planner) }
  let(:service_provider_account_type) { create(:account_type, name: :service_provider) }
  let!(:owner_role) { create(:role, name: :owner) }
  let!(:contributor_role) { create(:role, name: :contributor) }
  let!(:admin_user) { create(:user, is_admin: true) }
  let!(:companies) { create_list :company, 5 }
  let!(:non_membership_company ) { create :company }
  let!(:admin_company_memberships) do
    admin_company_memberships = []
    companies.each do |company|
      admin_company_memberships << create(:company_membership, user: admin_user, company: company, role: owner_role)
    end
    admin_company_memberships
  end

  let!(:managed_user) { create(:user, manager_id: admin_user.id) }
  let!(:managed_user_memberships) do
    companies.each do |company|
      create(:company_membership, user: managed_user, company: company, role: contributor_role)
    end
  end

  let!(:non_managed_user) { create(:user) }
  let!(:non_managed_user_memberships) do
    non_admin_company_memberships = []
    companies.each do |company|
      non_admin_company_memberships << create(:company_membership, user: non_managed_user, company: company, role: contributor_role)
    end
    non_admin_company_memberships
  end

  subject { described_class }

  permissions ".scope" do
    let!(:company_membership_policy_scope) { subject::Scope.new(admin_user, CompanyMembership).resolve }

    it "should return company memberships associated to 'owned' companies and managed users" do
      expect(company_membership_policy_scope.count).to eq(10)
      expect(company_membership_policy_scope.pluck(:company_id).uniq).to match_array(companies.pluck(:id))
      expect(company_membership_policy_scope.pluck(:user_id)).to include(admin_user.id, managed_user.id)
      expect(company_membership_policy_scope.pluck(:user_id)).not_to include(non_managed_user.id)
    end
  end

  permissions :index? do
    context "as a admin user" do
      it "should be true" do
        policy = subject.new(admin_user, CompanyMembership)
        expect(policy.index?).to be_truthy
      end
    end

    context "as a non admin user" do
      it "should be false" do
        policy = subject.new(managed_user, CompanyMembership)
        expect(policy.index?).to be_falsey
      end
    end
  end

  permissions :show? do
    context "as an admin user" do
      it "should be true" do
        policy = subject.new(admin_user, managed_user.company_memberships.first)
        expect(policy.show?).to be_truthy
      end

      it "should be false" do
        policy = subject.new(admin_user, non_managed_user.company_memberships.first)
        expect(policy.show?).to be_falsey
      end
    end

    context "as a non admin user" do
      it "should be false" do
        policy = subject.new(managed_user, CompanyMembership)
        expect(policy.show?).to be_falsey
      end
    end
  end

  permissions :create? do
    context "as an admin user" do
      let!(:event_planning_agency) { create(:company, company_type: create(:company_type, name: :event_planning_agency)) }
      let!(:event_planning_company) { create(:company, company_type: create(:company_type, name: :event_planning_company)) }
      let!(:service_provider_agency) { create(:company, company_type: create(:company_type, name: :service_provider_agency)) }
      let!(:service_provider_company) { create(:company, company_type: create(:company_type, name: :service_provider_company)) }

      let!(:event_planner_managed_user) { create(:user, manager_id: admin_user.id, account_type: event_planner_account_type) }
      let!(:service_provider_managed_user) { create(:user, manager_id: admin_user.id, account_type: service_provider_account_type) }

      let!(:admin_epa_membership) { create(:company_membership, user: admin_user, company: event_planning_agency, role: owner_role)}
      let!(:admin_epc_membership) { create(:company_membership, user: admin_user, company: event_planning_company, role: owner_role)}
      let!(:admin_spa_membership) { create(:company_membership, user: admin_user, company: service_provider_agency, role: owner_role)}
      let!(:admin_spc_membership) { create(:company_membership, user: admin_user, company: service_provider_company, role: owner_role)}

      it "should return true when a user account_type is event_planner and company_type is event planning company/agency" do
        membership = build(:company_membership, company: event_planning_agency, user: event_planner_managed_user)
        policy = subject.new(admin_user, membership)
        expect(policy.create?).to be_truthy

        membership2 = build(:company_membership, company: event_planning_company, user: event_planner_managed_user)
        policy2 = subject.new(admin_user, membership2)
        expect(policy2.create?).to be_truthy
      end

      it "should return true when a user account_type is service_provider and company_type is service provider company/agency" do
        membership = build(:company_membership, company: service_provider_agency, user: service_provider_managed_user)
        policy = subject.new(admin_user, membership)
        expect(policy.create?).to be_truthy

        membership2 = build(:company_membership, company: service_provider_company, user: service_provider_managed_user)
        policy2 = subject.new(admin_user, membership2)
        expect(policy2.create?).to be_truthy
      end

      it "should be false with a user that is not the admin's managed user" do
        policy = subject.new(admin_user, build(:company_membership, company: companies.first, user: non_managed_user))
        expect(policy.create?).to be_falsey
      end

      it "should be false if the admin user doesn't have a company membership with the owner role" do
        policy = subject.new(admin_user, build(:company_membership, company: non_membership_company, user: managed_user))
        expect(policy.create?).to be_falsey
      end

      it "should be false when user is not a managed user and admin doesn't have an owner membership for the company" do
        policy = subject.new(admin_user, build(:company_membership, company: non_membership_company, user: non_managed_user))
        expect(policy.create?).to be_falsey
      end

    end

    context "as a non admin user" do
      it "should be false" do
        policy = subject.new(managed_user, CompanyMembership)
        expect(policy.create?).to be_falsey
      end
    end
  end

  permissions :update?, :destroy? do
    context "as an admin user" do
      it "should return true when user is managed_user and admin has owner memership with company" do
        policy = subject.new(admin_user, managed_user.company_memberships.first)
        expect(policy.update?).to be_truthy
        expect(policy.destroy?).to be_truthy
      end

      it "should return false when not a managed_user's membership" do
        policy = subject.new(admin_user, non_managed_user.company_memberships.first)
        expect(policy.update?).to be_falsey
        expect(policy.destroy?).to be_falsey
      end

      it "should return false when not a company that admin user has an owner membership with" do
        policy = subject.new(admin_user, build(:company_membership, company: non_membership_company, role: contributor_role))
        expect(policy.update?).to be_falsey
        expect(policy.destroy?).to be_falsey
      end
    end

    context "as a non admin user" do
      it "should be false" do
        policy = subject.new(managed_user, CompanyMembership)
        expect(policy.update?).to be_falsey
        expect(policy.destroy?).to be_falsey
      end
    end
  end

end
