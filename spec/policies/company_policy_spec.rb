require 'rails_helper'

RSpec.describe CompanyPolicy do

  let!(:event_planning_account_type) { create(:account_type, name: :event_planning) }
  let!(:service_provider_account_type) { create(:account_type, name: :service_provider) }
  let!(:admin_user) { create(:user, is_admin: true) }
  let!(:owner_role) { create(:role, name: :owner) }
  let!(:companies) { create_list :company, 5 }
  let!(:child_companies) { create_list :company, 5, parent_id: companies.first.id }
  let!(:non_membership_company ) { create :company }
  let!(:admin_user_memberships) do
    companies.each do |company|
      create(:company_membership, user: admin_user, company: company, role: owner_role)
    end
  end
  let!(:admin_user_memberships_2) do
    child_companies.each do |company|
      create(:company_membership, user: admin_user, company: company, role: owner_role)
    end
  end

  let!(:non_admin_user) { create(:user) }
  let!(:non_admin_user_memberships) do
    companies.each do |company|
      create(:company_membership, user: non_admin_user, company: company, role: owner_role)
    end
  end

  subject { described_class }

  permissions ".scope" do
    context "as an admin user" do
      let!(:company_policy_scope) { subject::Scope.new(admin_user, Company).resolve }

      it "should return membership companies" do
        expect(company_policy_scope.count).to eq(10)
        expect(company_policy_scope.pluck(:id)).to match_array((companies.pluck(:id) << child_companies.pluck(:id)).flatten)
      end
    end

    context "as a non admin user" do
      let!(:company_policy_scope) { subject::Scope.new(non_admin_user, Company).resolve }

      it "should return membership companies" do
        expect(company_policy_scope.count).to eq(5)
        expect(company_policy_scope.pluck(:id)).to match_array(companies.pluck(:id))
        expect(company_policy_scope.pluck(:id)).not_to include(*child_companies.pluck(:id))
      end
    end
  end

  permissions :show? do
    context "as an admin user" do
      it "should be true" do
        policy = subject.new(admin_user, companies.first)
        expect(policy.show?).to be_truthy
      end

      it "should be false" do
        policy = subject.new(admin_user, non_membership_company)
        expect(policy.show?).to be_falsey
      end
    end

    context "as a non admin user" do
      it "should be true" do
        policy = subject.new(non_admin_user, companies.first)
        expect(policy.show?).to be_truthy
      end

      it "should be false" do
        policy = subject.new(non_admin_user, non_membership_company)
        expect(policy.show?).to be_falsey
      end
    end
  end

  permissions :create?, :update, :destroy do
    context "as an admin user" do

      let!(:service_provider_account_type) { create(:account_type, name: :service_provider) }
      let!(:service_provider_admin_user) { create(:user, is_admin: true, account_type: service_provider_account_type) }
      let!(:service_provider_company_type) { create(:company_type, name: :service_provider_company) }
      let!(:service_provider_company) { build(:company, company_type: service_provider_company_type) }
      let!(:service_provider_agency_type) { create(:company_type, name: :service_provider_agency) }
      let!(:service_provider_agency) { build(:company, company_type: service_provider_agency_type) }

      it "should be true when new company is a service_provider or service_provider_agency" do
        policy = subject.new(service_provider_admin_user, service_provider_company)
        policy2 = subject.new(service_provider_admin_user, service_provider_company)
        expect(policy.create? && policy2.create?).to be_truthy
        expect(policy.update? && policy2.update?).to be_truthy
        expect(policy.destroy?).to be_truthy
      end

      let!(:event_planner_account_type) { create(:account_type, name: :event_planner) }
      let!(:event_planner_admin_user) { create(:user, is_admin: true, account_type: event_planner_account_type) }
      let!(:event_planning_company_type) { create(:company_type, name: :event_planning_company) }
      let!(:event_planning_company) { build(:company, company_type: event_planning_company_type) }
      let!(:event_planning_agency_type) { create(:company_type, name: :event_planning_agency) }
      let!(:event_planning_agency) { build(:company, company_type: event_planning_agency_type) }
      let!(:sp_admin_user_company_membership) { create(:company_membership, user: event_planner_admin_user, company: companies.first, role: owner_role) }

      it "should be true when new company is a event_planing_company or event_planning_agency" do
        policy = subject.new(service_provider_admin_user, service_provider_company)
        policy2 = subject.new(service_provider_admin_user, service_provider_company)
        expect(policy.create? && policy2.create?).to be_truthy
        expect(policy.update? && policy2.update?).to be_truthy
        expect(policy.destroy?).to be_truthy
      end

      it "should be true when new company has no parent_id" do
        policy = subject.new(
          service_provider_admin_user,
          build(:company, company_type: service_provider_company_type, parent_id: nil)
        )
        expect(policy.create?).to be_truthy
        expect(policy.update?).to be_truthy
        expect(policy.destroy?).to be_truthy
      end

      it "should be false when new company doesn't have has an invalid company_type" do
        policy = subject.new(
          service_provider_admin_user,
          build(:company, company_type: create(:company_type), parent_id: non_membership_company.id)
        )
        expect(policy.create?).to be_falsey
        expect(policy.update?).to be_falsey
        expect(policy.destroy?).to be_falsey
      end

      let!(:sp_admin_user_company_membership) { create(:company_membership, user: service_provider_admin_user, company: companies.first, role: owner_role) }

      it "should be true when new company has parent_id that is included in the user's memberships" do
        policy = subject.new(
          service_provider_admin_user,
          build(:company, company_type: service_provider_company_type, parent_id: companies.first.id)
        )
        expect(policy.create?).to be_truthy
        expect(policy.update?).to be_truthy
        expect(policy.destroy?).to be_truthy
      end

      it "should be false when new company has parent_id that is not included in the user's memberships" do
        policy = subject.new(
          service_provider_admin_user,
          build(:company, company_type: service_provider_company_type, parent_id: non_membership_company.id)
        )
        expect(policy.create?).to be_falsey
        expect(policy.update?).to be_falsey
        expect(policy.destroy?).to be_falsey
      end
    end

    context "as a non admin user" do
      it "should be false" do
        policy = subject.new(non_admin_user, build(:company))
        expect(policy.create?).to be_falsey
        expect(policy.update?).to be_falsey
        expect(policy.destroy?).to be_falsey
      end
    end
  end
end
