require 'rails_helper'

RSpec.describe UserPolicy do

  let!(:admin_user) { create(:user, is_admin: true) }
  let!(:admin_user_with_no_managed_users) { create(:user, is_admin: true) }
  let!(:managed_users) { create_list(:user, 5, manager_id: admin_user.id) }
  let!(:non_managed_user) { create :user }

  subject { described_class }

  permissions ".scope" do
    context "as an admin user" do
      let!(:user_policy_scope) { subject::Scope.new(admin_user, User).resolve }

      it "should return managed_users" do
        expect(user_policy_scope.count).to eq(6)
        expect(user_policy_scope.pluck(:id)).to match_array(managed_users.pluck(:id) << admin_user.id)
      end
    end

    context "as a non admin user" do
      let!(:user_policy_scope) { subject::Scope.new(non_managed_user, User).resolve }

      it "should return results that only include non_managed_user" do
        expect(user_policy_scope.count).to eq(1)
        expect(user_policy_scope.first.id).to eq(non_managed_user.id)
      end
    end
  end

  permissions :create? do
    context "as an admin user" do
      it "should be true" do
        policy = subject.new(admin_user, User)
        expect(policy.create?).to be_truthy
      end
    end

    context "as a non admin user" do
      it "should be false" do
        policy = subject.new(non_managed_user, User)
        expect(policy.create?).to be_falsey
      end
    end
  end

  permissions :show?, :update? do
    context "as an admin user" do
      it "should be true if user is a managed_user" do
        policy = subject.new(admin_user, managed_users.first)
        expect(policy.show?).to be_truthy
        expect(policy.update?).to be_truthy
      end

      it "should be true if it is the same user" do
        policy = subject.new(admin_user, admin_user)
        expect(policy.show?).to be_truthy
        expect(policy.update?).to be_truthy
      end

      it "should be false if user is not a managed_user" do
        policy = subject.new(admin_user, non_managed_user)
        expect(policy.show?).to be_falsey
        expect(policy.update?).to be_falsey
      end
    end

    context "as a non admin user" do
      it "should be true if it is the same user" do
        policy = subject.new(non_managed_user, non_managed_user)
        expect(policy.show?).to be_truthy
        expect(policy.update?).to be_truthy
      end

      it "should be false if it is not the same user" do
        policy = subject.new(non_managed_user, managed_users.first)
        expect(policy.show?).to be_falsey
        expect(policy.update?).to be_falsey
      end
    end
  end

  permissions :create? do
    context "as an admin user" do
      it "should be true" do
        expect(subject.new(admin_user, User).create?).to be_truthy
      end
    end

    context "as a non admin user" do
      it "should be false" do
        expect(subject.new(non_managed_user, User).create?).to be_falsey
      end
    end
  end

  permissions :destroy? do
    context "as an admin user" do
      it "should be true if user is a managed_user" do
        expect(subject.new(admin_user, managed_users.first).destroy?).to be_truthy
      end

      it "should be true if user is the admin user and no managed_users exist" do
        expect(subject.new(admin_user_with_no_managed_users, admin_user_with_no_managed_users).destroy?).to be_truthy
      end

      it "should be false if user is the admin user and managed_users exist" do
        expect(subject.new(admin_user, admin_user).destroy?).to be_falsey
      end

      it "should be false if user is not a managed_user" do
        expect(subject.new(admin_user, non_managed_user).destroy?).to be_falsey
      end
    end

    context "as a non admin user" do
      it "should be false" do
        expect(subject.new(non_managed_user, non_managed_user).destroy?).to be_falsey
        expect(subject.new(non_managed_user, managed_users.first).destroy?).to be_falsey
        expect(subject.new(non_managed_user, non_managed_user).destroy?).to be_falsey
      end
    end
  end
end
