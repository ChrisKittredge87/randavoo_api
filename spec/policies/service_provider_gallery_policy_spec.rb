require 'rails_helper'

RSpec.describe ServiceProviderGalleryPolicy do

  subject { described_class }

  permissions ".scope" do
    let!(:service_provider_galleries) { create_list(:service_provider_gallery, 5) }
    let!(:policy_scope) { subject::Scope.new(nil, ServiceProviderGallery).resolve }

    it "should return all service provider galleries" do
      expect(policy_scope.count).to eq 5
      expect(policy_scope.pluck(:id)).to match_array(service_provider_galleries.pluck(:id))
    end
  end

  permissions :index?, :show? do
    it "should be true with or without a user" do
      policy = subject.new(build(:user), ServiceProviderGallery)
      expect(policy.index?).to be_truthy
      expect(policy.show?).to be_truthy

      policy2 = subject.new(build(:user), ServiceProviderGallery)
      expect(policy2.index?).to be_truthy
      expect(policy2.show?).to be_truthy

      policy3 = subject.new(nil, nil)
      expect(policy3.index?).to be_truthy
      expect(policy3.show?).to be_truthy
    end
  end

  permissions :create?, :update?, :destroy do
    let!(:company) { create(:company) }
    let!(:service_provider) { create(:service_provider, company: company) }
    let!(:service_provider_gallery) { create(:service_provider_gallery, service_provider: service_provider) }

    let!(:owner_user) { create(:user) }
    let!(:contributor_user) { create(:user) }
    let!(:reader_user) { create(:user) }
    let!(:no_role_user) { create(:user) }
    let!(:owner_role) { create(:role, name: :owner) }
    let!(:contributor_role) { create(:role, name: :contributor) }
    let!(:reader_role) { create(:role, name: :reader) }
    let!(:company_membership_1) { create(:company_membership, company: company, user: owner_user, role: owner_role) }
    let!(:company_membership_2) { create(:company_membership, company: company, user: contributor_user, role: contributor_role) }
    let!(:company_membership_3) { create(:company_membership, company: company, user: reader_user, role: reader_role) }

    it "should be true with a user who has owner role in service_provider company" do
      policy = subject.new(owner_user, service_provider_gallery)
      expect(policy.create?).to be_truthy
      expect(policy.update?).to be_truthy
      expect(policy.destroy?).to be_truthy
    end

    it "should be false with a user who does not have owner role in service_provider company" do
      [contributor_user, reader_user, no_role_user].each do |u|
        policy = subject.new(u, service_provider_gallery)
        expect(policy.create?).to be_falsey
        expect(policy.update?).to be_falsey
        expect(policy.destroy?).to be_falsey
      end
    end

    it "should be false for nil user" do
      policy = subject.new(nil, build(:service_provider_gallery))
      expect(policy.create?).to be_falsey
      expect(policy.update?).to be_falsey
      expect(policy.destroy?).to be_falsey
    end
  end
end
