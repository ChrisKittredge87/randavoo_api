require 'rails_helper'

RSpec.describe ServiceProviderReviewPolicy do

  let!(:admin_user) { create :user, is_admin: true }
  let!(:non_admin_user) { create :user }

  let!(:reviews) { create_list :service_provider_review, 5 }

  subject { described_class }

  permissions ".scope" do
    context "as an admin user" do
      let!(:policy_scope) { subject::Scope.new(admin_user, ServiceProviderReview).resolve }

      it "should return managed_users" do
        expect(policy_scope.count).to eq(5)
      end
    end

    context "as a non admin user" do
      let!(:policy_scope) { subject::Scope.new(non_admin_user, ServiceProviderReview).resolve }

      it "should return empty array" do
        expect(policy_scope.count).to eq(5)
      end
    end

    context "without a user" do
      let!(:policy_scope) { subject::Scope.new(nil, ServiceProviderReview).resolve }

      it "should return empty array" do
        expect(policy_scope.count).to eq(5)
      end
    end
  end

  permissions :index?, :show? do
    context "as an admin user" do
      it "should be true" do
        policy = subject.new(admin_user, ServiceProviderReview)
        expect(policy.index?).to be_truthy
        expect(policy.show?).to be_truthy
      end
    end

    context "as a non admin user" do
      it "should be true" do
        policy = subject.new(non_admin_user, ServiceProviderReview)
        expect(policy.index?).to be_truthy
        expect(policy.show?).to be_truthy
      end
    end

    context "without a user" do
      it "should be true" do
        policy = subject.new(nil, ServiceProviderReview)
        expect(policy.index?).to be_truthy
        expect(policy.show?).to be_truthy
      end
    end
  end
end
