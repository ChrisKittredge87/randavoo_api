require 'rails_helper'

RSpec.describe EventTemplatePolicy do

  let!(:user) { create :user }

  subject { described_class }

  permissions ".scope" do
    let!(:event_template_1) { create :event_template, user: user }
    let!(:scope) { subject::Scope.new(user, EventTemplate).resolve }

    it "should return event templates that belong to a user" do
      expect(scope.count).to eq(1)
      expect(scope.pluck(:user_id).uniq).to eq([user[:id]])
    end
  end

  permissions :index?, :show? do
    context "when user exists" do
      let(:policy) { subject.new(user, EventTemplate) }
      it "should return true" do
        expect(policy.index?).to be_truthy
        expect(policy.show?).to be_truthy
      end
    end

    context "when user is nil" do
      let(:policy) { subject.new(nil, EventTemplate) }
      it "should retrun false" do
        expect(policy.index?).to be_falsey
        expect(policy.show?).to be_falsey
      end
    end
  end

  permissions :create?, :update?, :destroy? do
    context "when a user is an event planner with correct ownership of an event template" do
      let!(:ep_account_type) { create :account_type, name: :event_planner }
      let!(:ep_user) { create :user, account_type: ep_account_type }
      let!(:company) { create :company }
      let!(:role) { create :role, name: :owner }
      let!(:company_membership) { create :company_membership, user: ep_user, company: company, role: role }
      let!(:event_template) { create :event_template, user: ep_user, company: company }
      let(:policy) { subject.new(event_template.user, event_template) }
      it "should return true" do
        expect(policy.create?).to be_truthy
        expect(policy.update?).to be_truthy
        expect(policy.destroy?).to be_truthy
      end
    end

    context "when a user is not an event planner account type" do
      let!(:non_ep_account_type) { create :account_type, name: :service_provider }
      let!(:non_ep_user) { create :user, account_type: non_ep_account_type }
      let(:policy) { subject.new(non_ep_user, EventTemplate) }
      it "should return false" do
        expect(policy.create?).to be_falsey
        expect(policy.update?).to be_falsey
        expect(policy.destroy?).to be_falsey
      end
    end

    context "when a user is an event planner that does not have a company membership" do
      let!(:ep_account_type) { create :account_type, name: :event_planner }
      let!(:ep_user) { create :user, account_type: ep_account_type }
      let!(:company) { create :company }
      let!(:event_template) { create :event_template, user: ep_user, company: company }
      let(:policy) { subject.new(event_template.user, event_template) }
      it "should return false" do
        expect(policy.create?).to be_falsey
        expect(policy.update?).to be_falsey
        expect(policy.destroy?).to be_falsey
      end
    end

    context "when a user is not the user that created this template" do
      let!(:ep_account_type) { create :account_type, name: :event_planner }
      let!(:ep_user) { create :user, account_type: ep_account_type }
      let!(:company) { create :company }
      let!(:event_template) { create :event_template, user: user, company: company }
      let(:policy) { subject.new(event_template.user, event_template) }
      it "should return false" do
        expect(policy.create?).to be_falsey
        expect(policy.update?).to be_falsey
        expect(policy.destroy?).to be_falsey
      end
    end

    context "when a user is nil" do
      let(:policy) { subject.new(nil, EventTemplate) }
      it "should return false" do
        expect(policy.create?).to be_falsey
        expect(policy.update?).to be_falsey
        expect(policy.destroy?).to be_falsey
      end
    end
  end

end
