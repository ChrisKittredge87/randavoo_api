require 'rails_helper'

RSpec.describe ServiceProviderReviewCommentPolicy do

  let!(:admin_user) { create :user, is_admin: true }
  let!(:non_admin_user) { create :user }

  let!(:review_comments) { create_list :service_provider_review_comment, 5 }

  subject { described_class }

  permissions ".scope" do
    context "as an admin user" do
      let!(:policy_scope) { subject::Scope.new(admin_user, ServiceProviderReviewComment).resolve }

      it "should return managed_users" do
        expect(policy_scope.count).to eq(5)
      end
    end

    context "as a non admin user" do
      let!(:policy_scope) { subject::Scope.new(non_admin_user, ServiceProviderReviewComment).resolve }

      it "should return empty array" do
        expect(policy_scope.count).to eq(5)
      end
    end

    context "without a user" do
      let!(:policy_scope) { subject::Scope.new(nil, ServiceProviderReviewComment).resolve }

      it "should return empty array" do
        expect(policy_scope.count).to eq(5)
      end
    end
  end

  permissions :index?, :show? do
    context "as an admin user" do
      it "should be true" do
        policy = subject.new(admin_user, ServiceProviderReviewComment)
        expect(policy.index?).to be_truthy
        expect(policy.show?).to be_truthy
      end
    end

    context "as a non admin user" do
      it "should be true" do
        policy = subject.new(non_admin_user, ServiceProviderReviewComment)
        expect(policy.index?).to be_truthy
        expect(policy.show?).to be_truthy
      end
    end

    context "without a user" do
      it "should be true" do
        policy = subject.new(nil, ServiceProviderReviewComment)
        expect(policy.index?).to be_truthy
        expect(policy.show?).to be_truthy
      end
    end
  end

  permissions :create?, :update?, :destroy? do
    let!(:owner_role) { create :role, name: :owner }
    let!(:contributor_role) { create :role, name: :contributor }
    let!(:reader_role) { create :role, name: :reader }
    let!(:admin_owner_company_membership) { create :company_membership, user: admin_user, company: review_comments.first.service_provider_review.service_provider.company, role: owner_role }
    let!(:non_admin_owner_company_membership) { create :company_membership, user: non_admin_user, company: review_comments.first.service_provider_review.service_provider.company, role: owner_role }
    let!(:admin_contributor_company_membership) { create :company_membership, user: admin_user, company: review_comments.second.service_provider_review.service_provider.company, role: contributor_role }
    let!(:non_admin_contributor_company_membership) { create :company_membership, user: non_admin_user, company: review_comments.second.service_provider_review.service_provider.company, role: contributor_role }


    context "as an admin user" do
      it "should be true when user has owner company_membership" do
        policy = subject.new(admin_user, review_comments.first)
        expect(policy.create?).to be_truthy
        expect(policy.update?).to be_truthy
        expect(policy.destroy?).to be_truthy
      end

      it "should be false when user does not have owner company_membership" do
        policy = subject.new(admin_user, review_comments.second)
        expect(policy.create?).to be_falsey
        expect(policy.update?).to be_falsey
        expect(policy.destroy?).to be_falsey

        policy_2 = subject.new(admin_user, review_comments.third)
        expect(policy_2.create?).to be_falsey
        expect(policy_2.update?).to be_falsey
        expect(policy_2.destroy?).to be_falsey
      end
    end

    context "as a non admin user" do
      it "should be true" do
        policy = subject.new(non_admin_user, review_comments.first)
        expect(policy.create?).to be_truthy
        expect(policy.update?).to be_truthy
        expect(policy.destroy?).to be_truthy
      end

      it "should be false when user does not have owner company_membership" do
        policy = subject.new(non_admin_user, review_comments.second)
        expect(policy.create?).to be_falsey
        expect(policy.update?).to be_falsey
        expect(policy.destroy?).to be_falsey

        policy_2 = subject.new(non_admin_user, review_comments.third)
        expect(policy_2.create?).to be_falsey
        expect(policy_2.update?).to be_falsey
        expect(policy_2.destroy?).to be_falsey
      end
    end
  end
end
