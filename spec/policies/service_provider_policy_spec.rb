require 'rails_helper'

RSpec.describe ServiceProviderPolicy do

  let!(:admin_user) { create(:user, is_admin: true) }
  let!(:non_admin_user) { create(:user) }

  let!(:company) { create(:company) }
  let!(:service_providers) { create_list(:service_provider, 5, company: company) }

  subject { described_class }

  permissions ".scope" do
    let!(:policy_scope) { subject::Scope.new(non_admin_user, ServiceProvider).resolve }

    it "should return all service providers" do
      expect(policy_scope.count).to eq 5
      expect(policy_scope.pluck(:id)).to match_array(service_providers.pluck(:id))
    end
  end

  permissions :index?, :show? do
    it "should be true with or without a user" do
      policy = subject.new(non_admin_user, ServiceProvider)
      expect(policy.index?).to be_truthy
      expect(policy.show?).to be_truthy

      policy2 = subject.new(admin_user, ServiceProvider)
      expect(policy2.index?).to be_truthy
      expect(policy2.show?).to be_truthy

      policy3 = subject.new(nil, nil)
      expect(policy3.index?).to be_truthy
      expect(policy3.show?).to be_truthy
    end
  end

  describe "company membership requirements" do
    let!(:owner_role) { create(:role, name: :owner) }
    let!(:contributor_role) { create(:role, name: :contributor) }
    let!(:reader_role) { create(:role, name: :reader) }
    let!(:company_1) { create :company }
    let!(:company_2) { create :company }
    let!(:company_3) { create :company }

    let!(:admin_company_owner_membership) { create(:company_membership, user: admin_user, company: company_1, role: owner_role) }
    let!(:non_admin_company_owner_membership) { create(:company_membership, user: non_admin_user, company: company_1, role: owner_role) }
    let!(:non_admin_company_conrtibutor_membership) { create(:company_membership, user: non_admin_user, company: company_2, role: contributor_role) }
    let!(:non_admin_company_reader_membership) { create(:company_membership, user: non_admin_user, company: company_3, role: reader_role) }

    permissions :create?, :update? do
      it "should return true" do
        policy = subject.new(admin_user, build(:service_provider, company_id: company_1.id))
        expect(policy.create?).to be_truthy
        expect(policy.update?).to be_truthy

        policy_2 = subject.new(non_admin_user, build(:service_provider, company_id: company_1.id))
        expect(policy_2.create?).to be_truthy
        expect(policy_2.update?).to be_truthy
      end

      it "should return false" do
        policy = subject.new(admin_user, build(:service_provider, company_id: company_2.id))
        expect(policy.create?).to be_falsey
        expect(policy.update?).to be_falsey

        policy_2 = subject.new(non_admin_user, build(:service_provider, company_id: company_2.id))
        expect(policy_2.create?).to be_falsey
        expect(policy_2.update?).to be_falsey

        policy_3 = subject.new(non_admin_user, build(:service_provider, company_id: company_3.id))
        expect(policy_3.create?).to be_falsey
        expect(policy_3.update?).to be_falsey
      end
    end

    permissions :destroy? do
      it "should be true" do
        policy = subject.new(admin_user, build(:service_provider, company_id: company_1.id))
        expect(policy.destroy?).to be_truthy
      end

      it "should be false" do
        policy = subject.new(non_admin_user, build(:service_provider, company_id: company_1.id))
        expect(policy.destroy?).to be_falsey
      end
    end
  end
end
