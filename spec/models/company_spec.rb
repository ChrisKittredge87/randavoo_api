# == Schema Information
#
# Table name: companies
#
#  id              :integer          not null, primary key
#  name            :string(250)      not null
#  company_type_id :integer          not null
#  address_id      :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  parent_id       :integer
#
# Indexes
#
#  index_companies_on_address_id       (address_id)
#  index_companies_on_company_type_id  (company_type_id)
#  index_companies_on_name             (name)
#  index_companies_on_parent_id        (parent_id)
#
# Foreign Keys
#
#  fk_rails_...  (address_id => addresses.id)
#  fk_rails_...  (company_type_id => company_types.id)
#

require 'rails_helper'

RSpec.describe Company, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
