# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  first_name             :string(250)      not null
#  last_name              :string(250)      not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  auth_token             :string           default("")
#  token_expiry           :datetime         default(Sun, 09 Jul 2017 21:24:30 UTC +00:00), not null
#  is_admin               :boolean          default(FALSE)
#  account_type_id        :integer          not null
#  deleted_at             :datetime
#  datetime               :datetime
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  manager_id             :integer
#
# Indexes
#
#  index_users_on_account_type_id       (account_type_id)
#  index_users_on_auth_token            (auth_token) UNIQUE
#  index_users_on_deleted_at            (deleted_at)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_manager_id            (manager_id)
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (account_type_id => account_types.id)
#

require 'rails_helper'

RSpec.describe User, type: :model do

  before { @user = build(:user) }

  subject { @user }

  it { should respond_to(:email) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }

  it { should be_valid }

  describe "when email is not present" do
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email).ignoring_case_sensitivity }
    it { should validate_confirmation_of(:password) }
    it { should allow_value('example@domain.com').for(:email) }
  end

  # test that the user actually responds to this attribute
  it { should respond_to(:auth_token)}

  # test that the auth_token is unique
  it { should validate_uniqueness_of(:auth_token) }

  describe "#generate_authentication_token!" do
    it "generates a unique token" do
      Devise.stub(:friendly_token).and_return("auniquetoken123")
      @user.generate_authentication_token!
      expect(@user.auth_token).to eql "auniquetoken123"
    end

    it "generates another token when one aready has been taken" do
      existing_user = create(:user, auth_token: "auniquetoken123")
      @user.generate_authentication_token!
      expect(@user.auth_token).not_to eql existing_user.auth_token
    end
  end

end
