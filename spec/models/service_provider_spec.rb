# == Schema Information
#
# Table name: service_providers
#
#  id                       :integer          not null, primary key
#  name                     :string(250)
#  overview                 :string(5000)
#  travel_distance          :float
#  company_id               :integer          not null
#  service_provider_type_id :integer          not null
#  address_id               :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#
# Indexes
#
#  index_service_providers_on_address_id                (address_id)
#  index_service_providers_on_company_id                (company_id)
#  index_service_providers_on_service_provider_type_id  (service_provider_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (address_id => addresses.id)
#  fk_rails_...  (company_id => companies.id)
#  fk_rails_...  (service_provider_type_id => service_provider_types.id)
#

require 'rails_helper'

RSpec.describe ServiceProvider, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
