# == Schema Information
#
# Table name: service_provider_pictures
#
#  id                          :integer          not null, primary key
#  name                        :string(100)
#  description                 :string(250)
#  image_file_name             :string           not null
#  image_content_type          :string           not null
#  image_file_size             :integer          not null
#  image_updated_at            :datetime         not null
#  service_provider_gallery_id :integer
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  order                       :integer          default(0), not null
#
# Indexes
#
#  index_service_provider_pictures_on_service_provider_gallery_id  (service_provider_gallery_id)
#
# Foreign Keys
#
#  fk_rails_...  (service_provider_gallery_id => service_provider_galleries.id)
#

require 'rails_helper'

RSpec.describe ServiceProviderPicture, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
