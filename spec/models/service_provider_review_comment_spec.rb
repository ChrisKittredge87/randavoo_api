# == Schema Information
#
# Table name: service_provider_review_comments
#
#  id                         :integer          not null, primary key
#  text                       :string(2000)     not null
#  service_provider_review_id :integer
#  user_id                    :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#
# Indexes
#
#  index_service_provider_review_comments_on_user_id  (user_id)
#  index_sp_rc_on_sp_review_id                        (service_provider_review_id)
#
# Foreign Keys
#
#  fk_rails_...  (service_provider_review_id => service_provider_reviews.id)
#  fk_rails_...  (user_id => users.id)
#

require 'rails_helper'

RSpec.describe ServiceProviderReviewComment, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
