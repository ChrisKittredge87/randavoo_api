# == Schema Information
#
# Table name: service_provider_galleries
#
#  id                  :integer          not null, primary key
#  name                :string(100)      not null
#  description         :string(250)
#  service_provider_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_service_provider_galleries_on_service_provider_id  (service_provider_id)
#
# Foreign Keys
#
#  fk_rails_...  (service_provider_id => service_providers.id)
#

require 'rails_helper'

RSpec.describe ServiceProviderGallery, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
