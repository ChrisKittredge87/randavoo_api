# == Schema Information
#
# Table name: event_templates
#
#  id                          :integer          not null, primary key
#  user_id                     :integer          not null
#  company_id                  :integer          not null
#  dont_set_budget             :boolean          not null
#  start_date                  :datetime         not null
#  end_date                    :datetime         not null
#  number_of_expected_guests   :integer          not null
#  overall_budget              :float
#  lonlat                      :geography({:srid not null, point, 4326
#  address                     :string           not null
#  include_in_house_vendors    :boolean          not null
#  include_outside_vendors     :boolean          not null
#  needs_venue                 :boolean          not null
#  radius                      :integer
#  venue_budget                :float
#  venue_experience_id         :integer
#  vendor_budget               :float            not null
#  caterer_quantity            :integer          not null
#  budget_per_caterer          :float
#  food_truck_quantity         :integer          not null
#  budget_per_food_truck       :float
#  food_truck_paid_for_by_host :boolean
#  description                 :string(500)      not null
#  name                        :string(100)      not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_event_templates_on_company_id           (company_id)
#  index_event_templates_on_lonlat               (lonlat)
#  index_event_templates_on_user_id              (user_id)
#  index_event_templates_on_venue_experience_id  (venue_experience_id)
#
# Foreign Keys
#
#  fk_rails_...  (company_id => companies.id)
#  fk_rails_...  (user_id => users.id)
#  fk_rails_...  (venue_experience_id => venue_experiences.id)
#

require 'rails_helper'

RSpec.describe EventTemplate, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
