require 'rails_helper'

def mock_authorize(record, unauthorize: false)
  expectation = expect(controller).to receive(:authorize).with(record)
  expectation.and_raise(Pundit::NotAuthorizedError) if unauthorize

  allow(controller).to receive(:verify_authorized)
end

RSpec.describe Api::V1::RegistrationsController, type: :controller do
  let!(:role) { create(:role, name: :owner) }

  context "When creating an event planner user" do
    let!(:event_planner_account_type) { create(:account_type, name: :event_planner) }
    let!(:event_planning_company) { create(:company_type, name: :event_planning_company)}

    before(:each) do
      @user_attributes = attributes_for(:user)
        .merge!(account_type_id: event_planner_account_type[:id])
      post :create, params: { user: @user_attributes }
    end

    it "should create an event planner company that and add the user to that company" do
      expect(User.last.company_memberships.size).to eq(1)
    end
  end
end
