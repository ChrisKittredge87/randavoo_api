require 'rails_helper'

RSpec.describe Api::V1::CompaniesController, type: :controller do

  let!(:user) { create(:user) }
  let!(:companies) { create_list(:company, 5) }
  let!(:owner_role) { create(:role, name: :owner) }

  before(:each) do
    allow(controller).to receive(:authenticate_with_token!).and_return(true)
    allow(controller).to receive(:current_user).and_return(user)
    allow(controller).to receive(:authorize).and_return(true)
  end

  describe "#INDEX" do
    context "user has memberships to all companies" do
      before(:each) do
        companies.each do |company|
          create(:company_membership, user: user, company: company, role: owner_role)
        end
        get :index
      end

      it "gets companies" do
        expect(JSON.parse(response.body).count).to eq(5)
        expect(response.status).to eq(200)
      end
    end

    context "user has memberships to some companies" do
      before(:each) do
        create(:company_membership, user: user, company: companies.first, role: owner_role)
        create(:company_membership, user: user, company: companies.second, role: owner_role)
        get :index
      end

      it "gets companies" do
        results = JSON.parse(response.body, symbolize_names: true)
        expect(results.count).to eq(2)
        expect(results.find { |c| c[:id] == companies.first.id }).to_not be_nil
        expect(results.find { |c| c[:id] == companies.second.id }).to_not be_nil
        expect(results.find { |c| c[:id] == companies.third.id }).to be_nil
        expect(response.status).to eq(200)
      end
    end
  end

  describe "#SHOW" do
    context "user has membership to view company" do
      let!(:company) { companies.first }
      let!(:company_membership) { create(:company_membership, user: user, company: company, role: owner_role) }

      before(:each) do
        get :show, params: { id: company.id }
      end

      it "get company" do
        expect(JSON.parse(response.body)).to eq({
            "id" => company.id,
            "name" => company.name,
            "companyType"=> {
              "id" => company.company_type.id,
              "name" => company.company_type.name,
              "displayName" => company.company_type.display_name,
              "description" => company.company_type.description
            },
            "companyTypeId" => company.company_type.id,
            "parentId" => nil,
            "canContribute" => true,
            "canEdit" => true
        })
        expect(response.status).to eq(200)
      end

    end
  end

end
