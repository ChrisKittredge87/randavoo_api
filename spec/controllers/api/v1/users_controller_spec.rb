require 'rails_helper'

def mock_authorize(record, unauthorize: false)
  expectation = expect(controller).to receive(:authorize).with(record)
  expectation.and_raise(Pundit::NotAuthorizedError) if unauthorize

  allow(controller).to receive(:verify_authorized)
end

RSpec.describe Api::V1::UsersController, type: :controller do

  let!(:admin_user) { create(:user, is_admin: true) }

  before(:each) do
    allow(controller).to receive(:current_user).and_return(admin_user)
  end

  describe "GET #index" do
    let!(:users) { create_list(:user, 5, manager_id: admin_user.id) }
    before(:each) do
      allow(controller).to receive(:policy_scope).and_return(admin_user.managed_users)
      get :index
    end

    it "should get users" do
      expect(JSON.parse(response.body).count).to eq(5)
      expect(response.status).to eq(200)
    end
  end

  describe "GET #show" do
    context "user exists" do
      before(:each) do
        get :show, params: { id: admin_user.id }
      end
      it { should respond_with 200 }
    end

    context "user does not exists" do
      before(:each) do
        get :show, params: { id: 'invalid_id' }
      end
      it { should respond_with 404 }
    end
  end

  describe "POST #create" do
    context "when user is successfully created" do
      before(:each) do
        @user_attributes = attributes_for(:user).merge!(is_admin: true)
        post :create, params: { user: @user_attributes }
      end

      it "renders the json representation for the user record just created" do
        expect(json_response[:isAdmin]).to be_truthy
        expect(json_response[:email]).to eql @user_attributes[:email]
      end

      it { should respond_with 201 }
    end

    context "when user is not created" do
      before(:each) do
        post :create, params: { user: { missing: 'params' } }
      end

      it "renders an errors json" do
        expect(json_response).to have_key(:errors)
      end

      it "renders the json errors on why the user could not be created" do
        user_response = json_response
        expect(user_response[:errors][:email]).to include "can't be blank"
      end

      it { should respond_with 422 }
    end
  end

  describe "PUT/PATCH #update" do
    context "when is successfully updated by an admin user" do
      before(:each) do
        @user = create :user
        api_authorization_headers @user.auth_token
        patch :update, params: { id: @user.id, user: { email: "newmail@example.com", is_admin: true } }
      end

      it "renders the json representation for the updated user" do
        user_response = json_response
        expect(user_response[:email]).to eql "newmail@example.com"
        expect(user_response[:isAdmin]).to be_truthy
      end

      it { should respond_with 200 }
    end

    context "when is successfully updated by a non admin user" do
      let!(:non_admin_user) { create :user }
      before(:each) do
        allow(controller).to receive(:current_user).and_return(non_admin_user)
        @user = create :user
        patch :update, params: { id: @user.id, user: { is_admin: true } }
      end

      it "renders the json representation for the updated user" do
        user_response = json_response
        expect(user_response[:isAdmin]).to be_falsey
      end

      it { should respond_with 200 }
    end

    context "when is not updated" do
      before(:each) do
        @user = create :user
        api_authorization_headers @user.auth_token
        patch :update, params: { id: @user.id, user: { email: "bademail.com" }}
      end

      it "renders an errors json" do
        user_response = json_response
        expect(user_response).to have_key(:errors)
      end

      it "renders the json errors on whye the user could not be created" do
        user_response = json_response
        expect(user_response[:errors][:email]).to include "is invalid"
      end

      it { should respond_with 422 }
    end
  end

  describe "DELETE #destroy" do
    before(:each) do
      @user = create :user
      api_authorization_headers @user.auth_token
      delete :destroy, params: { id: @user.id }
    end

    it { should respond_with 200 }
  end

end
