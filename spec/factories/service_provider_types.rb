# == Schema Information
#
# Table name: service_provider_types
#
#  id           :integer          not null, primary key
#  name         :string(250)      not null
#  description  :string(500)      not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  display_name :string           not null
#

FactoryGirl.define do
  factory :service_provider_type do
    name { Faker::Lorem.word }
    display_name { Faker::Lorem.word }
    description { Faker::Lorem.sentence }
  end
end
