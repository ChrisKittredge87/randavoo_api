# == Schema Information
#
# Table name: roles
#
#  id           :integer          not null, primary key
#  name         :string           not null
#  description  :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  display_name :string(100)
#
# Indexes
#
#  index_roles_on_name  (name) UNIQUE
#

FactoryGirl.define do
  factory :role do
    name { Faker::Lorem.word }
    description { Faker::Lorem.sentence }
    display_name { Faker::Lorem.word }
  end
end
