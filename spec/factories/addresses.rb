# == Schema Information
#
# Table name: addresses
#
#  id               :integer          not null, primary key
#  street_address_1 :string(200)      not null
#  street_address_2 :string(200)
#  locality         :string(200)      not null
#  region           :string(200)      not null
#  postcode         :string(100)      not null
#  country          :string(200)      not null
#  lonlat           :geography({:srid point, 4326
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_addresses_on_lonlat  (lonlat)
#

FactoryGirl.define do
  factory :address do
    country { Faker::Address.country_code }
    administrative_area { Faker::Address.state_abbr }
    sub_administrative_area { Faker::Address.community }
    locality { Faker::Address.city }
    postal_code { Faker::Address.postcode }
    throughfare { Faker::Address.street_address }
    premise { Faker::Address.secondary_address }
  end
end
