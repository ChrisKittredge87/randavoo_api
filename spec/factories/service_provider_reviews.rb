# == Schema Information
#
# Table name: service_provider_reviews
#
#  id                  :integer          not null, primary key
#  rating              :integer          not null
#  header              :string(150)      not null
#  text                :string(2000)     not null
#  service_provider_id :integer          not null
#  user_id             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_service_provider_reviews_on_service_provider_id  (service_provider_id)
#  index_service_provider_reviews_on_user_id              (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (service_provider_id => service_providers.id)
#  fk_rails_...  (user_id => users.id)
#

FactoryGirl.define do
  factory :service_provider_review do
    association :service_provider, factory: :service_provider
    association :user, factory: :user
    rating { Faker::Number.between(1, 5) }
    header { Faker::Lorem.word }
    text { Faker::Lorem.sentence }
  end
end
