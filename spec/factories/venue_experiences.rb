# == Schema Information
#
# Table name: venue_experiences
#
#  id           :integer          not null, primary key
#  description  :string(500)
#  name         :string(100)      not null
#  display_name :string(100)      not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :venue_experience do
    description { Faker::Name.name }
    name { Faker::Name.name }
    display_name { Faker::Name.name }
  end
end
