# == Schema Information
#
# Table name: company_types
#
#  id           :integer          not null, primary key
#  name         :string(50)       not null
#  description  :string(150)      not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  display_name :string(100)
#

FactoryGirl.define do
  factory :company_type do
    name { Faker::Lorem.word }
    display_name { Faker::Lorem.word }
    description { Faker::Lorem.sentence }
  end
end
