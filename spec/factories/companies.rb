# == Schema Information
#
# Table name: companies
#
#  id              :integer          not null, primary key
#  name            :string(250)      not null
#  company_type_id :integer          not null
#  address_id      :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  parent_id       :integer
#
# Indexes
#
#  index_companies_on_address_id       (address_id)
#  index_companies_on_company_type_id  (company_type_id)
#  index_companies_on_name             (name)
#  index_companies_on_parent_id        (parent_id)
#
# Foreign Keys
#
#  fk_rails_...  (address_id => addresses.id)
#  fk_rails_...  (company_type_id => company_types.id)
#

FactoryGirl.define do
  factory :company do
    association :company_type, factory: :company_type
    name { Faker::Company.name }
  end
end
