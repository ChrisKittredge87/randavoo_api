# == Schema Information
#
# Table name: facebook_users
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  facebook_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_facebook_users_on_user_id  (user_id)
#

FactoryGirl.define do
  factory :facebook_user do
    references ""
    facebook_id ""
  end
end
