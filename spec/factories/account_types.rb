# == Schema Information
#
# Table name: account_types
#
#  id           :integer          not null, primary key
#  name         :string(50)       not null
#  description  :string(150)      not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  display_name :string(100)      not null
#

FactoryGirl.define do
  factory :account_type do
    name "MyString"
    description "MyString"
    display_name "MyString"
  end
end
