# == Schema Information
#
# Table name: event_template_cuisine_types
#
#  id                :integer          not null, primary key
#  cuisine_type_id   :integer
#  event_template_id :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_event_template_cuisine_types_on_cuisine_type_id    (cuisine_type_id)
#  index_event_template_cuisine_types_on_event_template_id  (event_template_id)
#  index_unique_event_template_cusines_types                (cuisine_type_id,event_template_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (cuisine_type_id => cuisine_types.id)
#  fk_rails_...  (event_template_id => event_templates.id)
#

FactoryGirl.define do
  factory :event_template_cuisine_type do
    cuisine_type nil
    event_template nil
  end
end
