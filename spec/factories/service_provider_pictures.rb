# == Schema Information
#
# Table name: service_provider_pictures
#
#  id                          :integer          not null, primary key
#  name                        :string(100)
#  description                 :string(250)
#  image_file_name             :string           not null
#  image_content_type          :string           not null
#  image_file_size             :integer          not null
#  image_updated_at            :datetime         not null
#  service_provider_gallery_id :integer
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  order                       :integer          default(0), not null
#
# Indexes
#
#  index_service_provider_pictures_on_service_provider_gallery_id  (service_provider_gallery_id)
#
# Foreign Keys
#
#  fk_rails_...  (service_provider_gallery_id => service_provider_galleries.id)
#

FactoryGirl.define do
  factory :service_provider_picture do
    association :service_provider_gallery, factory: :service_provider_gallery
    name { Faker::Lorem.word }
    description { Faker::Lorem.sentence }
    image_file_name { Faker::Lorem.word }
    image_content_type { Faker::File.mime_type }
    image_file_size { 500 }
    image_updated_at { Date.current }
  end
end
